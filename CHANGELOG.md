# Changelog

All notable changes to the project will be documented in this file.

## [0.3.3] (build 210204-0645)

### UI changes
* Added a tooltip to the player aid card ([BGA #28024](https://boardgamearena.com/bug?id=28024))

### Bug fixes
* Fixed the localization of some text in the round summary table ([BGA #28023](https://boardgamearena.com/bug?id=28023))

## [0.3.2] (build 200910-0919)

### UI changes
* Disabled debug borders for gold zones on cards in play
* Updated game banner from the publisher

## [0.3.1] (build 200819-0046)

### UI changes
* Added gold cubes to the cards as they are played to show which player won the duel
* Added the official player aid card to the bottom of the play area

### Bug fixes
* Fixed the last card in hand not being playable if it is one higher than the previous

## [0.2.1] - 2020-07-13

### Gameplay changes
* You can change your card selection if the opponent hasn't chosen theirs yet

### UI changes
* Added a highlight to the card slots where duels are expected in the current round
* Added a black outline to the player name text so the green name is more visible on the green background
* Added a separator between the duels in the round summary table
* Improved screen size detection to reduce cases where the player's hand of cards is partially hidden

### Bug fixes
* Fixed the round number not updating without a page refresh ([BGA #20677](https://boardgamearena.com/bug?id=20677))

## [0.1.1] - 2020-07-09

### UI changes
* Added a red glow to selected cards in hand so the selection is more obvious
* Added a box-shadow effect to cards in hand and in play
* Added a text-shadow to the round number for increased visibility
* Added whiteblock style to the player's hand
* Added padding to the gold resource icon in the player panel and aligned it vertically
* Updated game icon from the publisher

### Bug fixes
* Fixed the 1 card not allowed immediately after dynamite is played ([BGA #20557](https://boardgamearena.com/bug?id=20557))
* Fixed the card text sometimes appearing larger than expected and overflowing the text box ([BGA #20531](https://boardgamearena.com/bug?id=20531))

## [0.1.0] - 2020-07-08

### Alpha release!

[0.3.3]: https://bitbucket.org/Joel_L/fistfulofgold/commits/tag/v0.3.3
[0.3.2]: https://bitbucket.org/Joel_L/fistfulofgold/commits/tag/v0.3.2
[0.3.1]: https://bitbucket.org/Joel_L/fistfulofgold/commits/tag/v0.3.1
[0.2.1]: https://bitbucket.org/Joel_L/fistfulofgold/commits/tag/v0.2.1
[0.1.1]: https://bitbucket.org/Joel_L/fistfulofgold/commits/tag/v0.1.1
[0.1.0]: https://bitbucket.org/Joel_L/fistfulofgold/commits/tag/v0.1.0