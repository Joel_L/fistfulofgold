**Project status: Alpha**

This project is developed for use with the BGA Studio framework. It cannot be built, and does not function as a stand-alone entity.

Consult the [release notes](CHANGELOG.md) for a reader-friendly history of changes.

---

## TODO

These are remaining items that must be completed before the game is usable on BGA

* Implement the "show discarded cards" variant
  - place the cards below the hand in two rows (one per player)
  - the cardsDiscarded notification message should reveal the card or keep it hidden depending on the variant
  - choose this variant by default for new players

---

## NICE TO HAVE

These are ideas and improvements that aren't necessary but if time permits they will make the implementation better

* Improve visibility of the green player name
* Maybe place the chosen card on the table upside-down instead of writing "player chooses a card" in the replay log. Could flip it with a CSS 3D transform. https://3dtransforms.desandro.com/card-flip
* Revert all the ES3 changes and update the ESLint settings to ES6
* Remove the getPlayerColoredYouHtml function from JS and put the description in PHP, see https://forum.boardgamearena.com/viewtopic.php?f=12&t=16788
* Trim the borders from the card art
* Improve the progression calculator
* Use onScreenWidthChange callback for detecting layout size
* Switch to updateCounters for player panel resources (see JS reference page on BGA Studio) instead of innerHTML - could potentially do this for round_number too
* Need clearer animations in general - should not have to read the log to understand what's happening
* Move all non-BGA game logic to a separate file and add tests

---

## KNOWN ISSUES

These are known issues that hinder the play experience. Some of these are player-reported on the [BGA bugs and suggestions](https://boardgamearena.com/bugs?game=1232) page.

* Gold icon in the player panel didn't reset to 0 after the round ended ([BGA #20976](https://boardgamearena.com/bug?id=20976))
* Right edge of the play area is slightly clipped (~2px) on mobile
* "Gold", "Total", and "DYNAMITE" in the summary table aren't displayed as translatable strings (<< >>) in Studio. Een says it's missing _() on the JS side, but I don't do anything with it there...
* "Replay last move" shows the previous round's scoring table
* Replay cards are sometimes invisible (see the first duel of the last round in [this example](https://boardgamearena.com/archive/replay/200707-1235/?table=100360090&player=39141269&comments=41781731;)