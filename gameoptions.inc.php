<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * fistfulofgold implementation : © Joel Lieberman <joellie@live.com>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * gameoptions.inc.php
 *
 * fistfulofgold game options description
 * 
 * In this file, you can define your game options (= game variants).
 *   
 * Note: If your game has no variant, you don't have to modify this file.
 *
 * Note²: All options defined in this file should have a corresponding "game state labels"
 *        with the same ID (see "initGameStateLabels" in fistfulofgold.game.php)
 *
 * !! It is not a good idea to modify this file when a game is running !!
 *
 */

require_once("modules/fogdefines.inc.php");

$game_options = array(

    OPTION_GAME_LENGTH => array(
        'name' => totranslate("Game length"),
        'values' => array(

            GAME_LENGTH_SHORT => array(
                'name' => totranslate("Short (win 2 rounds)")),

            GAME_LENGTH_LONG => array(
                'name' => totranslate("Long (win 3 rounds)"))
        )
    )
);


