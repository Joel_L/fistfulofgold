<?php
 /**
  *------
  * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
  * fistfulofgold implementation : © Joel Lieberman <joellie@live.com>
  * 
  * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
  * See http://en.boardgamearena.com/#!doc/Studio for more information.
  * -----
  * 
  * fistfulofgold.game.php
  *
  * This is the main file for your game logic.
  *
  * In this PHP file, you are going to defines the rules of the game.
  *
  */


require_once(APP_GAMEMODULE_PATH.'module/table/table.game.php');
require_once("modules/fogdefines.inc.php");

if (!defined("GLOBAL_ROUND_NUMBER")) // ensure this block is only invoked once, since it is included multiple times
{
    // Game constants.
    define("NUM_CARD_SLOTS", 7);
    define("MAX_NUM_ROUNDS", 7);
    define("STARTING_HAND_SIZE", 7);
    define("STARTING_GOLD_IN_SUPPLY", 40);

    define("END_ROUND_NO", 1);
    define("END_ROUND_FINAL_DUEL", 2);
    define("END_ROUND_YES_WINNER", 3);
    define("END_ROUND_YES_DRAW", 4);

    // These are persistent variables stored in the global table of the database.
    define("GLOBAL_ROUND_NUMBER", "round_number");
    define("GLOBAL_DUEL_NUMBER", "duel_number");
    define("GLOBAL_SAVED_STATE", "saved_state");
}

class fistfulofgold extends Table
{
	function __construct()
	{
        // Your global variables labels:
        //  Here, you can assign labels to global variables you are using for this game.
        //  You can use any number of global variables with IDs between 10 and 99.
        //  If your game has options (variants), you also have to associate here a label to
        //  the corresponding ID in gameoptions.inc.php.
        // Note: afterwards, you can get/set the global variables with getGameStateValue/setGameStateInitialValue/setGameStateValue
        parent::__construct();
        
        self::initGameStateLabels(array(

            // Persisted global variables.
            GLOBAL_ROUND_NUMBER => 10,
            GLOBAL_DUEL_NUMBER => 11,
            GLOBAL_SAVED_STATE => 12,

            // Game variants.
            "game_length" => OPTION_GAME_LENGTH
       ));        

        $this->cards = self::getNew("module.common.deck");
        $this->cards->init("card");
	}
	
    protected function getGameName()
    {
		// Used for translations and stuff. Please do not modify.
        return "fistfulofgold";
    }	

    /*
        setupNewGame:
        
        This method is called only once, when a new game is launched.
        In this method, you must setup the game according to the game rules, so that
        the game is ready to be played.
    */
    protected function setupNewGame($players, $options = array())
    {    
        $gameinfos = self::getGameinfos();
        $default_colors = $gameinfos['player_colors'];
 
        // Create players
        // Note: if you added some extra field on "player" table in the database (dbmodel.sql), you can initialize it there.
        $sql = "INSERT INTO player (player_id, player_color, player_canal, player_name, player_avatar) VALUES ";
        $values = array();
        $cards = array();
        foreach ($players as $player_id => $player)
        {
            $color = array_shift($default_colors);
            $values[] = "('".$player_id."','$color','".$player['player_canal']."','".addslashes($player['player_name'])."','".addslashes($player['player_avatar'])."')";

            // Create the player decks.
            foreach ($this->card_defs_by_color[$color] as $type_id)
            {
                $card = $this->card_defs[$type_id];
                $cards[$player_id][] = array('type' => $card['name'], 'type_arg' => $type_id, 'nbr' => 1);
            }

            // Each player has their own deck.
            $this->cards->createCards($cards[$player_id], "deck", $player_id);
        }
        $sql .= implode($values, ",");
        self::DbQuery($sql);

        // Create the slots that will hold gold in tied duels.
        $sql = "INSERT INTO gold_zone (slot_num, player_id) VALUES ";
        $values = array();
        foreach (range(1, $this->getNumCardSlots()) as $slot_num)
        {
            $values[] = "(" . $slot_num . ", 0)";

            foreach ($players as $player_id => $player)
            {
                $values[] = "(" . $slot_num . ", " . $player_id . ")";
            }
        }
        $sql .= implode($values, ",");
        self::DbQuery($sql);

        
        /************ Start the game initialization *****/

        // Init global values with their initial values
        self::setGameStateInitialValue(GLOBAL_ROUND_NUMBER, 1);
        self::setGameStateInitialValue(GLOBAL_DUEL_NUMBER, 1);
        self::setGameStateInitialValue(GLOBAL_SAVED_STATE, 0);
        
        // Init game statistics (defined in stats.inc.php)
        self::initStat("table", STAT_TABLE_DUEL_COUNT, 0);
        self::initStat("table", STAT_TABLE_DUEL_TIE_COUNT, 0);
        self::initStat("table", STAT_TABLE_FINAL_DUEL_COUNT, 0);
        self::initStat("table", STAT_TABLE_ROUND_COUNT, 0);
        self::initStat("table", STAT_TABLE_ROUND_DRAW_COUNT, 0);
        self::initStat("player", STAT_PLAYER_DUEL_WIN_COUNT, 0);
        self::initStat("player", STAT_PLAYER_GOLD_TOTAL, 0);
        self::initStat("player", STAT_PLAYER_GOLD_FROM_TIES, 0);

        // Activate first player (which is in general a good idea :))
        $this->activeNextPlayer();

        /************ End of the game initialization *****/
    }

    /*
        getAllDatas: 
        
        Gather all informations about current game situation (visible by the current player).
        
        The method is called each time the game interface is displayed to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)
    */
    protected function getAllDatas()
    {
        $result = array();
    
        $current_player_id = self::getCurrentPlayerId();    // !! We must only return informations visible by this player !!

        // Current round and duel numbers.
        $result['round_number'] = $this->getRoundNumber();
        $result['duel_number'] = $this->getDuelNumber();
    
        // Get information about players
        // Note: you can retrieve some extra field you added for "player" table in "dbmodel.sql" if you need it.
        $sql = "SELECT player_id id, player_color color, player_score score, player_gold gold, player_chosen_card_id chosen_card_id FROM player";
        $result['players'] = self::getCollectionFromDb($sql);
  
        // All possible cards.
        $result['card_defs'] = $this->card_defs;
        
        // Cards in player hand      
        $result['my_hand'] = $this->cards->getCardsInLocation("hand", $current_player_id);
  
        // Cards on the table
        $result['cards_in_play'] = array();
        foreach ($result['players'] as $player_id => $player)
            $result['cards_in_play'][$player_id] = $this->cards->getCardsInLocation("play_area_" . $player_id);

        // Gold on the table from tied duels.
        $result['gold_on_table'] = $this->getAllGoldOnTable();

        // Gold remaining in the general supply. (Although not a limited resource.)
        $total_gold_in_play = 0;
        foreach ($this->getAllGoldOnTable() as $slot_num => $gold_zones)
        {
            foreach ($gold_zones as $player_id => $gold_zone)
            {
                $gold = (int)$gold_zone['gold'];
                $total_gold_in_play += $gold;
            }
        }
        $result['gold_in_supply'] = max($this->getStartingGoldInSupply() - $total_gold_in_play, 0);

        // Expected number of slots that will have cards played in this round. It could be wrong if there's a final duel or if Red 7 is played.
        $result['expected_num_duels'] = $this->getExpectedNumDuelsInCurrentRound();

        return $result;
    }

    /*
        getGameProgression:
        
        Compute and return the current game progression.
        The number returned must be an integer beween 0 (=the game just started) and
        100 (= the game is finished or almost finished).
    
        This method is called each time we are in a game state with the "updateGameProgression" property set to true 
        (see states.inc.php)
    */
    function getGameProgression()
    {
        $stars_awarded_count = 0;
        $players = self::loadPlayersBasicInfos();

        foreach ($players as $player_id => $player)
        {
            $stars_awarded_count += $this->getPlayerNumStars($player_id);
        }

        $total_possible_stars = $this->getNumStarsToWinGame() * count($players) - 1;
        $num_tied_rounds = max($this->getRoundNumber() - $total_possible_stars, 0);
        $expected_num_rounds = $total_possible_stars + $num_tied_rounds;

        $expected_duel_count = 0;
        $current_duel_count = 0;

        for ($i = 1; $i <= $expected_num_rounds; $i++)
        {
            $num_duels_in_round_i = $this->getStartingHandSize() - $i;
            $expected_duel_count += $num_duels_in_round_i;

            if ($i < $this->getRoundNumber())
                $current_duel_count += $num_duels_in_round_i;
        }
        
        $current_duel_count += $this->getDuelNumber() - 1;
        $normalized_progress = $current_duel_count / $expected_duel_count;

        return min($normalized_progress * 100, 100);
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Utility functions
////////////    

    /*
        In this space, you can put any utility methods useful for your game logic
    */

    // Check a condition that is expected to fail sometimes as part of normal gameplay (e.g. the player clicked the wrong area of the board,
    // or tried to spend the wrong amount of resources for an action).
    function userAssert($assertCondition, $messageForUser)
    {
        if (!$assertCondition)
        {
            throw new BgaUserException($messageForUser);
        }
    }

    // If this fails, the client did something the server thinks is impossible. Either the client is modified or there's a bug in the code.
    function systemAssert($assertCondition, $messageToLog)
    {
        if (!$assertCondition)
        {
            self::error($messageToLog);
            throw new BgaVisibleSystemException(self::_("Invalid action. Try refreshing the page. If this happens again please report a bug."));
        }
    }

    function getPlayerByColor($player_color)
    {
        $players = self::loadPlayersBasicInfos();

        foreach ($players as $player_id => $player)
        {
            if ($player['player_color'] == $player_color)
                return $player;
        }

        return null;
    }

    function getPlayerName($player_id)
    {
        $players = self::loadPlayersBasicInfos();
        return $players[$player_id]['player_name'];
    }

    function getNumCardSlots()
    {
        return NUM_CARD_SLOTS;
    }

    function getExpectedNumDuelsInCurrentRound()
    {
        return max($this->getNumCardSlots() - $this->getRoundNumber(), 1);
    }

    function getStartingHandSize()
    {
        return STARTING_HAND_SIZE;
    }

    function getStartingGoldInSupply()
    {
        return STARTING_GOLD_IN_SUPPLY;
    }

    function getNumStarsToWinGame()
    {
        $num_stars_to_win_game = 2;

        if ($this->gamestate->table_globals[OPTION_GAME_LENGTH] == GAME_LENGTH_LONG)
            $num_stars_to_win_game = 3;
        
        return $num_stars_to_win_game;
    }

    function getRoundNumber()
    {
        return (int)self::getGameStateValue(GLOBAL_ROUND_NUMBER);
    }

    function isLastPossibleRound()
    {
        return $this->getRoundNumber() == MAX_NUM_ROUNDS;
    }

    function incrementRoundNumber()
    {
        self::incGameStateValue(GLOBAL_ROUND_NUMBER, 1);
    }

    function getDuelNumber()
    {
        return (int)self::getGameStateValue(GLOBAL_DUEL_NUMBER);
    }

    function incrementDuelNumber()
    {
        self::incGameStateValue(GLOBAL_DUEL_NUMBER, 1);
    }

    function resetDuelNumber()
    {
        self::setGameStateValue(GLOBAL_DUEL_NUMBER, 1);
    }

    function validateDiscardCards($player_id, $card_ids_to_discard)
    {
        foreach ($card_ids_to_discard as $card_id_to_discard)
        {
            $card_to_discard = $this->cards->getCard($card_id_to_discard);
            $card_to_discard_type_id = $card_to_discard['type_arg'];

            $this->systemAssert(
                $card_to_discard['location'] == "hand" && $card_to_discard['location_arg'] == $player_id,
                sprintf("Player ID %s tried to discard card ID %s (type %s) but it isn't in their hand", $player_id, $card_id_to_discard, $card_to_discard_type_id)); // NOI18N
        }
    }

    function isPlayerCardValueExactly1HigherThanPreviousDuel($player_id, $card_type_id)
    {
        $it_is = false;

        if ($this->getDuelNumber() > 1)
        {
            $card_value = $this->card_defs[$card_type_id]['value'];

            $previous_cards_played = $this->cards->getCardsInLocation("play_area_" . $player_id, $this->getDuelNumber() - 1);
            $previous_card_played = reset($previous_cards_played); // get the first element of the array
            $previous_card_type_id = $previous_card_played['type_arg'];
            $previous_card_value = $this->card_defs[$previous_card_type_id]['value'];

            $it_is = $card_value == $previous_card_value + 1;
        }
        
        return $it_is;
    }

    function validatePlayCard($player_id, $card_id_to_play)
    {
        $card_to_play = $this->cards->getCard($card_id_to_play);
        $card_to_play_type_id = $card_to_play['type_arg'];

        $this->systemAssert(
            $card_to_play['location'] == "hand" && $card_to_play['location_arg'] == $player_id,
            sprintf("Player ID %s tried to play card ID %s (type %s) but it isn't in their hand", $player_id, $card_id_to_play, $card_to_play_type_id)); // NOI18N

        $num_cards_in_hand = $this->cards->countCardInLocation("hand", $player_id);

        if ($num_cards_in_hand > 1)
        {
            $this->userAssert(
                !$this->isPlayerCardValueExactly1HigherThanPreviousDuel($player_id, $card_to_play_type_id),
                self::_("You can't play a card with a value 1 higher than the previous duel"));
        }
    }

    function validateReclaimDiscardedCard($player_id, $card_id_to_reclaim)
    {
        $card_to_reclaim = $this->cards->getCard($card_id_to_reclaim);
        $card_to_reclaim_type_id = $card_to_reclaim['type_arg'];

        $this->systemAssert(
            $card_to_reclaim['location'] == "discard" && $card_to_reclaim['location_arg'] == $player_id,
            sprintf("Player ID %s tried to reclaim card ID %s (type %s) but it isn't in their discard", $player_id, $card_id_to_reclaim, $card_to_reclaim_type_id)); // NOI18N
    }

    // Returns the card ID most recently chosen by the specified player
    function getChosenCardId($player_id)
    {
        $sql = "SELECT player_chosen_card_id FROM player WHERE player_id = $player_id";
        return self::getUniqueValueFromDB($sql);
    }

    // Sets the chosen card ID for the specified player.
    function setChosenCardId($player_id, $card_id)
    {
        $sql = "UPDATE player SET player_chosen_card_id = $card_id WHERE player_id = $player_id";
        self::DbQuery($sql);
    }

    function resetChosenCardId($player_id)
    {
        $this->setChosenCardId($player_id, 0);
    }

    function doesDynamiteWinAgainstCardValue($card_value)
    {
        return $card_value <= 4;
    }

    function isDynamite($card_type_id)
    {
        return
            $card_type_id == CARD_TYPE_RED_DYNAMITE ||
            $card_type_id == CARD_TYPE_GREEN_DYNAMITE;
    }

    function checkRed8VersusGreen1($chosenCardPlayerPairs)
    {        
        $winning_index = -1;

        if ($chosenCardPlayerPairs[0]['card_type_id'] == CARD_TYPE_RED_8 &&
            $chosenCardPlayerPairs[1]['card_type_id'] == CARD_TYPE_GREEN_1)
        {
            $winning_index = 0;
        }
        else if ($chosenCardPlayerPairs[1]['card_type_id'] == CARD_TYPE_RED_8 &&
                 $chosenCardPlayerPairs[0]['card_type_id'] == CARD_TYPE_GREEN_1)
        {
            $winning_index = 1;
        }

        return $winning_index;
    }

    function determineDuelResult($chosenCardPlayerPairs)
    {
        $winning_index = -1;
        $winning_player_id = 0;
        $winning_gold = 0;
        $round_is_forfeit = false;

        // Check for automatic round loss if the player plays a card value one higher than the previous duel.
        // This can only happen in the final duel. The game will prevent it otherwise. See validatePlayCard().
        for ($i = 0; $i < count($chosenCardPlayerPairs); $i++)
        {
            $player_id = $chosenCardPlayerPairs[$i]['player_id'];
            $card_type_id = $chosenCardPlayerPairs[$i]['card_type_id'];

            if ($this->isPlayerCardValueExactly1HigherThanPreviousDuel($player_id, $card_type_id))
            {
                // Illegal move. The round ends immediately.
                if ($round_is_forfeit)
                {
                    // Special case: both players were forced to play illegal cards in the final duel. The round is a draw.
                    $winning_index = -1;
                }
                else
                {
                    // The other player wins.
                    $winning_index = abs($i - 1);
                    $round_is_forfeit = true;
                }
            }                    
        }

        $card_values = array(
            $this->card_defs[$chosenCardPlayerPairs[0]['card_type_id']]['value'],
            $this->card_defs[$chosenCardPlayerPairs[1]['card_type_id']]['value']
        );

        $diff_between_card_values = abs($card_values[0] - $card_values[1]);

        if (!$round_is_forfeit)
        {
            if ($diff_between_card_values == 0)
            {
                // The duel is tied. Some gold will be placed between the cards (see getTiedGoldAmount()).
            }
            else if ($this->isDynamite($chosenCardPlayerPairs[0]['card_type_id']))
            {
                $winning_index = $this->doesDynamiteWinAgainstCardValue($card_values[1]) ? 0 : 1;
                $winning_gold = 3;
            }
            else if ($this->isDynamite($chosenCardPlayerPairs[1]['card_type_id']))
            {
                $winning_index = $this->doesDynamiteWinAgainstCardValue($card_values[0]) ? 1 : 0;
                $winning_gold = 3;
            }
            else
            {
                $winning_gold = $diff_between_card_values;

                if ($diff_between_card_values <= 3)
                {
                    // Highest card wins.
                    if ($card_values[0] > $card_values[1])
                        $winning_index = 0;
                    else
                        $winning_index = 1;
                }
                else
                {
                    $r8g1_winning_index = $this->checkRed8VersusGreen1($chosenCardPlayerPairs);
                    if ($r8g1_winning_index >= 0)
                    {
                        // Special case: Red 8 trumps Green 1 with a 3 gold reward.
                        $winning_index = $r8g1_winning_index;
                        $winning_gold = 3;
                    }
                    else
                    {
                        // Lowest card wins.
                        if ($card_values[0] < $card_values[1])
                            $winning_index = 0;
                        else
                            $winning_index = 1;
                    }
                }
            }
        }

        if ($winning_index >= 0)
            $winning_player_id = $chosenCardPlayerPairs[$winning_index]['player_id'];
        
        return array(
            'winning_player_id' => $winning_player_id,
            'winning_gold' => $winning_gold,
            'duel_is_tied' => $diff_between_card_values == 0,
            'round_is_forfeit' => $round_is_forfeit);
    }

    function getPlayerWhoPlayedFirstSixCard()
    {
        $first_six_player_id = 0;
        $first_six_slot_num = $this->getNumCardSlots() + 1;

        $players = self::loadPlayersBasicInfos();
        foreach ($players as $player_id => $player)
        {
            $cards_in_play = $this->cards->getCardsInLocation("play_area_" . $player_id);

            foreach ($cards_in_play as $card)
            {
                $card_type_id = $card['type_arg'];
                $card_slot_num = $card['location_arg'];

                if ($card_type_id == CARD_TYPE_GREEN_6 || $card_type_id == CARD_TYPE_RED_6)
                {
                    if ($first_six_player_id > 0 && $first_six_slot_num == $card_slot_num)
                    {
                        // Special case: both players played their 6 in the same duel. Ignore its ability.
                        $first_six_player_id = 0;
                    }
                    else if ($card_slot_num < $first_six_slot_num)
                    {
                        $first_six_player_id = $player_id;
                        $first_six_slot_num = $card_slot_num;    
                    }
                        
                    break;
                }
            }
        }

        return $first_six_player_id;
    }

    function getCardSlotNum($card_type_id)
    {
        $found_card_slot_num = $this->getNumCardSlots() + 1;

        $players = self::loadPlayersBasicInfos();
        foreach ($players as $player_id => $player)
        {
            $cards_in_play = $this->cards->getCardsInLocation("play_area_" . $player_id);

            foreach ($cards_in_play as $card)
            {
                $card_slot_num = $card['location_arg'];

                if ($card['type_arg'] == $card_type_id)
                {
                    $found_card_slot_num = $card_slot_num;
                    break;
                }
            }    
        }

        return $found_card_slot_num;
    }

    function evaluateCardAbility6()
    {
        // The first player to play a 6 wins all ties.
        $first_six_player_id = $this->getPlayerWhoPlayedFirstSixCard();

        if ($first_six_player_id > 0)
        {
            foreach ($this->getAllGoldOnTable() as $slot_num => $gold_zones)
            {
                foreach ($gold_zones as $player_id => $gold_zone)
                {
                    $gold = (int)$gold_zone['gold'];

                    if ($player_id == 0 && $gold > 0) // player_id is 0 if the gold is in the middle between the two cards, which is where the tied gold goes
                    {
                        $this->awardGoldToPlayer($first_six_player_id, $gold);
                        $this->removeAllGoldFromSlot($slot_num);

                        self::notifyAllPlayers("ability6", clienttranslate('${player_name} takes ${gold} gold from duel ${slot_num} (6 ability)'), array(
                            'player_id' => $first_six_player_id,
                            'player_name' => $this->getPlayerName($first_six_player_id),
                            'gold' => $gold,
                            'slot_num' => $slot_num
                        ));

                        // Record the bonus gold awarded so it can be displayed in the scoring summary table at the end of the round.
                        $this->updateDuelResultBonusGold($this->getRoundNumber(), $slot_num, $first_six_player_id, $gold);

                        self::incStat($gold, STAT_PLAYER_GOLD_TOTAL, $first_six_player_id);
                        self::incStat($gold, STAT_PLAYER_GOLD_FROM_TIES, $first_six_player_id);        
                    }
                }
            }
        }
    }

    function evaluateCardAbilityGreen7($duel_number, $winning_player_id)
    {
        $green_player = $this->getPlayerByColor(PLAYER_COLOR_GREEN);

        // The player who played Green 7 gets a bonus gold in each round they win after that one.
        if ($winning_player_id == $green_player['player_id'])
        {
            if ($duel_number > $this->getCardSlotNum(CARD_TYPE_GREEN_7))
            {
                $gold = 1;
                $green_player_id = $green_player['player_id'];

                $this->awardGoldToPlayer($green_player_id, $gold);

                self::notifyAllPlayers("abilityGreen7", clienttranslate('${player_name} earns ${gold} bonus gold (green 7 ability)'), array(
                    'player_id' => $green_player_id,
                    'player_name' => $green_player['player_name'],
                    'gold' => $gold,
                    'duel_number' => $duel_number
                ));

                    // Record the bonus gold awarded so it can be displayed in the scoring summary table at the end of the round.
                    $this->updateDuelResultBonusGold($this->getRoundNumber(), $this->getDuelNumber(), $green_player_id, $gold);

                self::incStat($gold, STAT_PLAYER_GOLD_TOTAL, $green_player_id);
            }
        }
    }

    function awardStarToPlayer($player_id)
    {
        $sql = "UPDATE player SET player_score = player_score + 1 WHERE player_id = $player_id";
        self::DbQuery($sql);
    }

    function getPlayerNumStars($player_id)
    {        
        $sql = "SELECT player_score FROM player WHERE player_id = $player_id";
        return self::getUniqueValueFromDB($sql);
    }

    function getTiedGoldAmount()
    {
        return 2;
    }

    function awardGoldToPlayer($player_id, $gold)
    {
        $sql = "UPDATE player SET player_gold = player_gold + $gold WHERE player_id = $player_id";
        self::DbQuery($sql);
    }

    function getPlayerGold($player_id)
    {
        $sql = "SELECT player_gold FROM player WHERE player_id = $player_id";
        return self::getUniqueValueFromDB($sql);
    }

    function resetPlayerGold($player_id)
    {
        $sql = "UPDATE player SET player_gold = 0 WHERE player_id = $player_id";
        self::DbQuery($sql);
    }

    function addGoldToCard($player_id, $slot_num, $gold)
    {
        $sql = "UPDATE gold_zone SET gold = $gold WHERE slot_num = $slot_num AND player_id = $player_id";
        self::DbQuery($sql);
    }

    function addGoldToSlot($slot_num, $gold)
    {
        $sql = "UPDATE gold_zone SET gold = $gold WHERE slot_num = $slot_num AND player_id = 0";
        self::DbQuery($sql);
    }

    function removeAllGoldFromSlot($slot_num)
    {
        $sql = "UPDATE gold_zone SET gold = 0 WHERE slot_num = $slot_num AND player_id = 0";
        self::DbQuery($sql);
    }

    function removeGoldFromAllSlots()
    {
        $sql = "UPDATE gold_zone SET gold = 0";
        self::DbQuery($sql);
    }

    function getAllGoldOnTable()
    {
        $sql = "SELECT slot_num, player_id, gold FROM gold_zone";
        return self::getDoubleKeyCollectionFromDB($sql);
    }

    function insertDuelResult($green_card_type_id, $red_card_type_id, $winning_player_id, $winning_gold)
    {
        $sql = "INSERT INTO duel (round_num, duel_num, green_card_type_id, red_card_type_id, winning_player_id, winning_gold) VALUES ";
        $values = array(
            $this->getRoundNumber(),
            $this->getDuelNumber(),
            $green_card_type_id,
            $red_card_type_id,
            $winning_player_id,
            $winning_gold
        );

        $sql .= "(" . implode($values, ",") . ")";
        self::DbQuery($sql);
    }

    function updateDuelResultBonusGold($round_num, $duel_num, $player_id, $gold)
    {
        $sql = "UPDATE duel
                SET
                    bonus_player_id = $player_id,
                    bonus_gold = $gold
                WHERE
                    round_num = $round_num AND
                    duel_num = $duel_num";

        self::DbQuery($sql);
    }

    function getDuelResultsForRound($round_num)
    {
        $sql = "SELECT duel_num, green_card_type_id, red_card_type_id, winning_player_id, winning_gold, bonus_player_id, bonus_gold FROM duel WHERE round_num = $round_num";
        $results = self::getCollectionFromDb($sql);

        // Sort the results by duel number.
        ksort($results, SORT_NUMERIC);

        return $results;
    }

    function isRed7InDuel3OrGreater()
    {
        $red_seven_slot_num = $this->getCardSlotNum(CARD_TYPE_RED_7);

        return
            $red_seven_slot_num >= 3 &&
            $red_seven_slot_num <= $this->getNumCardSlots();
    }

    function wasGreen8PlayedInDuel($duel_number)
    {
        return $this->getCardSlotNum(CARD_TYPE_GREEN_8) == $duel_number;
    }

    function wasGreen8PlayedInRound()
    {
        return $this->getCardSlotNum(CARD_TYPE_GREEN_8) <= $this->getNumCardSlots();
    }

    function handleEndOfRound($round_is_ending_early)
    {
        self::incStat(1, STAT_TABLE_ROUND_COUNT);

        // Show a scoring dialog summarizing the round.
        $this->showRoundScoringDialog();

        // Handle end of game.
        $end_game_result = $this->checkEndOfGame();
        $is_game_over = $end_game_result['is_game_over'];
        $winning_player_id = $end_game_result['winning_player_id'];

        if ($is_game_over)
        {
            if ($winning_player_id > 0)
            {
                self::notifyAllPlayers("gameResult", clienttranslate('${player_name} wins the game!'), array(
                    'player_name' => $this->getPlayerName($winning_player_id)
                ));    
            }
            else
            {
                self::notifyAllPlayers("gameResult", clienttranslate('No cards left. The game is over.'), array());    
            }
            
            $this->gamestate->nextState("gameEnd");
        }
        else // end the round
        {                
            $this->incrementRoundNumber();

            if ($round_is_ending_early)
            {
                // Players choose which card to discard.
                $this->setNextState(STATE_MULTI_DISCARD_CARDS);
            }
            else
            {
                $this->setNextState(STATE_GAME_ROUND_START);
            }
        }
    }    

    function showRoundScoringDialog()
    {
        /* This method generates a scoring dialog that looks like this:
            Round Summary
                                Round 3
                    <green player>	<red player>
            Duel 1	        7	            7
            Gold		                   + 2
            Duel 2	        5	            4
            Gold	      1 + 1	
            Duel 3	        4           	6
            Gold		                    2
            Duel 4	        3           	3
            Gold		                   + 2
            Total	        2           	6
        */
        $header_row = array("");

        // Green player is always shown first.
        $green_player = $this->getPlayerByColor(PLAYER_COLOR_GREEN);
        $green_player_id = $green_player['player_id'];

        $header_row[] = array(
            'str' => '${player_name}',
            'args' => array('player_name' => $green_player['player_name']),
            'type' => "header"
        );

        $red_player = $this->getPlayerByColor(PLAYER_COLOR_RED);
        $red_player_id = $red_player['player_id'];

        $header_row[] = array(
            'str' => '${player_name}',
            'args' => array('player_name' => $red_player['player_name']),
            'type' => "header"
        );

        $table = array();
        $table[] = $header_row;

        // Keep track of each player's total gold so it can be displayed as the last row of the table.
        $green_player_total_gold = 0;
        $red_player_total_gold = 0;

        // Generate the summary table by iterating over the duel results. They are already sorted in increasing order by duel number.
        $results = $this->getDuelResultsForRound($this->getRoundNumber());
        foreach ($results as $duel_num => $result)
        {
            // Each result is displayed in two rows. The first row shows the duel number and which cards were played by each player.
            $first_row_label = array(
                'str' => clienttranslate('Duel ${duel_num}'),
                'args' => array('duel_num' => $duel_num)
            );

            $green_card_type_id = $result['green_card_type_id'];
            $red_card_type_id = $result['red_card_type_id'];

            $green_card = $this->card_defs[$green_card_type_id];
            $red_card = $this->card_defs[$red_card_type_id];

            $first_row = array(
                $first_row_label,
                array(
                    'str' => clienttranslate($green_card['name']),
                    'args' => array('argument' => "empty")), // dummy arg to force translation
                array(
                    'str' => clienttranslate($red_card['name']),
                    'args' => array('argument' => "empty")), // dummy arg to force translation
            );

            $second_row_label = array(
                'str' => clienttranslate("Gold"),
                'args' => array('argument' => "empty")); // dummy arg to force translation

            $winning_player_id = $result['winning_player_id'];
            $winning_gold = $result['winning_gold'];

            $bonus_player_id = $result['bonus_player_id'];
            $bonus_gold = $result['bonus_gold'];
            $bonus_gold_string = $bonus_gold > 0 ? " + " . $bonus_gold : "";

            // The second row shows the gold awarded, in the column of the player who won it.
            $second_row = array();
            if ($winning_player_id == $green_player_id)
            {
                $second_row = array($second_row_label, $winning_gold . $bonus_gold_string, "");
                $green_player_total_gold += $winning_gold + $bonus_gold;
            }
            else if ($winning_player_id == $red_player_id)
            {
                $second_row = array($second_row_label, "", $winning_gold . $bonus_gold_string);
                $red_player_total_gold += $winning_gold + $bonus_gold;
            }
            else
            {
                if ($bonus_player_id == $green_player_id)
                {
                    $second_row = array($second_row_label, $bonus_gold_string, "");
                    $green_player_total_gold += $bonus_gold;
                }
                else if ($bonus_player_id == $red_player_id)
                {
                    $second_row = array($second_row_label, "", $bonus_gold_string);
                    $red_player_total_gold += $bonus_gold;
                }
                else
                {
                    $second_row = array($second_row_label, "", "");
                }
            }

            $table[] = $first_row;
            $table[] = $second_row;
        }

        $footer_row = array(
            array(
                'str' => clienttranslate("Total"),
                'args' => array('argument' => "empty")), // dummy arg to force translation
            $green_player_total_gold, $red_player_total_gold);

        $table[] = $footer_row;

        $this->notifyAllPlayers("tableWindow", "", array(
            'id' => "roundScoring",
            'title' => clienttranslate('Round Summary'),
            'table' => $table,
            'header' => array (
                'str' => clienttranslate('Round ${round_number}'),
                'args' => array('round_number' => $this->getRoundNumber())),
            'footer' => "",
            'closing' => clienttranslate("Close")
        ));         
    }

    function checkEndOfRound()
    {
        $end_round_status = END_ROUND_NO;
        $is_ending_early = false;
        $winning_player_id = 0;
        $winning_gold = 0;

        $num_cards_in_hand = $this->cards->countCardInLocation("hand");

        if ($num_cards_in_hand <= 3 || $this->isRed7InDuel3OrGreater()) // Red 7 causes the round to end early if played in the third duel or later
        {
            if ($num_cards_in_hand > 3)
                $is_ending_early = true;

            $players = self::loadPlayersBasicInfos();
            $players_gold = array();

            foreach ($players as $player_id => $player)
            {
                $player_gold = $this->getPlayerGold($player_id);
                $players_gold[] = $player_gold;

                if ($player_gold > $winning_gold)
                {
                    $winning_player_id = $player_id;
                    $winning_gold = $player_gold;    
                }
            }

            if ($players_gold[0] == $players_gold[1])
            {
                if ($num_cards_in_hand <= 1 ||          // players are still tied after the final duel; or
                    $this->isRed7InDuel3OrGreater())    // no final duel because Red 7 ability was triggered
                {
                    if ($num_cards_in_hand > 1)
                        $is_ending_early = true;

                    // The whole round is a draw.
                    $end_round_status = END_ROUND_YES_DRAW;
                }
                else
                {
                    // If there's a tie at the end of a round, perform a final duel with each player's last card instead of discarding it.
                    $end_round_status = END_ROUND_FINAL_DUEL;
                }
            }
            else
            {
                $end_round_status = END_ROUND_YES_WINNER;
            }
        }

        return array(
            'status' => $end_round_status,
            'is_ending_early' => $is_ending_early,
            'winning_player_id' => $winning_player_id,
            'winning_gold' => $winning_gold);
    }

    function checkEndOfGame()
    {
        $is_game_over = false;
        $winning_player_id = 0;

        $players = self::loadPlayersBasicInfos();
        foreach ($players as $player_id => $player)
        {
            $player_num_stars = $this->getPlayerNumStars($player_id);

            if ($player_num_stars == $this->getNumStarsToWinGame())
            {
                $is_game_over = true;
                $winning_player_id = $player_id;
            }
        }

        // Special case: the players have tied so many rounds there are no cards left. The whole game is a draw.
        if ($this->isLastPossibleRound())
            $is_game_over = true;

        return array(
            'is_game_over' => $is_game_over,
            'winning_player_id' => $winning_player_id);
    }

    function setNextState($state_id)
    {
        self::setGameStateValue(GLOBAL_SAVED_STATE, $state_id);
    }

    function transitionToNextState()
    {
        $transitionName = "";
        $state_id = (int)self::getGameStateValue(GLOBAL_SAVED_STATE);

        switch ($state_id)
        {
            case STATE_MULTI_PLAYER_TURN:
                $players = self::loadPlayersBasicInfos();
                foreach ($players as $player_id => $player)
                    self::giveExtraTime($player_id);

                $transitionName = "nextPlayer";
                break;

            case STATE_MULTI_DISCARD_CARDS:
                $transitionName = "discardCards";
                break;

            case STATE_PLAYER_RECLAIM_DISCARDED_CARD:
                $transitionName = "reclaimDiscardedCard";
                break;

            case STATE_GAME_ROUND_START:
                $transitionName = "nextRound";

                $players = self::loadPlayersBasicInfos();
                foreach ($players as $player_id => $player)
                {
                    // Automatically discard each player's last card (or last two cards if Green 8 was played).
                    $discarded_cards = $this->cards->getCardsInLocation("hand", $player_id);
                    $this->cards->moveAllCardsInLocation("hand", "discard", $player_id, $player_id);
                    
                    self::notifyPlayer($player_id, "cardsDiscarded_Private", "", array(
                        'card_ids' => array_map(function($card) { return $card['id']; }, array_values($discarded_cards))
                    ));
                }

                break;
        }

        $this->systemAssert(strlen($transitionName) > 0, sprintf("Unexpected transition to state %s", $state_id)); // NOI18N

        $this->gamestate->nextState($transitionName);
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Player actions
//////////// 

    /*
        Each time a player is doing some game action, one of the methods below is called.
        (note: each method below starting with "act" must match an input method in fistfulofgold.action.php)
    */

    function actDiscardCards($card_ids)
    {
        $player_id = self::getCurrentPlayerId();

        // Check if the action is allowed by game rules.
        self::checkAction("discardCards");
        $this->validateDiscardCards($player_id, $card_ids);

        // Perform the action.
        $this->discardCards($player_id, $card_ids);
    }

    function discardCards($player_id, $card_ids)
    {
        // Remove the chosen cards from their deck.
        $this->cards->moveCards($card_ids, "discard", $player_id);

        // Notify all players that cards were discarded.
        self::notifyAllPlayers("cardsDiscarded", clienttranslate('${player_name} discarded ${num_cards} cards'), array(
            'player_id' => $player_id,
            'player_name' => $this->getPlayerName($player_id),
            'num_cards' => count($card_ids),
        ));

        // Notify the player performing the discard that the action was approved by the server and they should remove the cards from their hand.
        self::notifyPlayer($player_id, "cardsDiscarded_Private", "", array(
            'card_ids' => $card_ids
        ));

        // Next state is usually the next duel (after initial setup), but if the round ended early due to Red 7, start the next round instead.
        $next_state = "nextRound";
        if ($this->gamestate->state()['args']['is_initial_setup'])
            $next_state = "nextPlayer";            

        // Deactivate this player. If none left, transition to next state.
        $this->gamestate->setPlayerNonMultiactive($player_id, $next_state);
    }

    function actPlayCard($card_id)
    {
        $player_id = self::getCurrentPlayerId();

        // Check if the action is allowed by game rules.
        $this->gamestate->checkPossibleAction("playCard"); // using checkPossibleAction instead of checkAction to allow a non-active player to change their mind and choose a different card
        $this->validatePlayCard($player_id, $card_id);
        
        // Perform the action.
        $this->playCard($player_id, $card_id);
    }

    function playCard($player_id, $card_id)
    {
        // If a card was already chosen by this player they are changing their choice.
        $is_first_card_chosen_by_this_player = $this->getChosenCardId($player_id) == 0;

        // Choose the card.
        $this->setChosenCardId($player_id, $card_id);

        if ($is_first_card_chosen_by_this_player)
        {
            // Notify all players that a card was chosen.
            self::notifyAllPlayers("cardChosen", clienttranslate('${player_name} chooses a card'), array(
                'player_id' => $player_id,
                'player_name' => $this->getPlayerName($player_id),
            ));        
        }
        
       // Deactivate this player. If none left, transition to next state.
       $this->gamestate->setPlayerNonMultiactive($player_id, "gameTurn");
    }

    function actReclaimDiscardedCard($card_id)
    {
        $player_id = self::getActivePlayerId();

        // Check if the action is allowed by game rules.
        self::checkAction("reclaimDiscardedCard"); 
        $this->validateReclaimDiscardedCard($player_id, $card_id);
    
        // Perform the action.
        $this->reclaimDiscardedCard($player_id, $card_id);
    }

    function reclaimDiscardedCard($player_id, $card_id)
    {
        $card_name = $this->cards->getCard($card_id)['type'];
        $card_type_id = $this->cards->getCard($card_id)['type_arg'];

        // Move the chosen card back to their hand.
        $this->cards->moveCard($card_id, "hand", $player_id);

        // Notify all players that a card was reclaimed.
        $players = self::loadPlayersBasicInfos();
        self::notifyAllPlayers("cardReclaimed", clienttranslate('${player_name} returned ${card_name} to his hand (green 8 ability)'), array(
            'player_id' => $player_id,
            'player_name' => $this->getPlayerName($player_id),
            'card_id' => $card_id,
            'card_name' => $card_name,
            'card_type_id' => $card_type_id,
            'slot_num' => $this->getNumCardSlots()
        ));        
        
        $this->transitionToNextState();
    }

    
//////////////////////////////////////////////////////////////////////////////
//////////// Game state arguments
////////////

    /*
        Here, you can create methods defined as "game state arguments" (see "args" property in states.inc.php).
        These methods function is to return some additional information that is specific to the current
        game state.
    */
    function argMultiDiscardCards()
    {
        // This state is a forced discard. There are two ways to arrive here.
        // 1. During initial setup each player discards 2 cards.
        // 2. Red 7 ends the round early and instructs each player to discard.

        $is_initial_setup = false;
        $args = array();
        $players = self::loadPlayersBasicInfos();
        
        foreach ($players as $player_id => $player)
        {
            $num_cards_in_hand = $this->cards->countCardInLocation("hand", $player_id);

            // Normally Red 7 has each player discard 1 card.
            $num_cards_to_discard = 1;

            if ($num_cards_in_hand > $this->getStartingHandSize())
            {
                // As part of initial setup, each player discards 2 cards to reach the starting hand size.
                $num_cards_to_discard = $num_cards_in_hand - $this->getStartingHandSize();
                $is_initial_setup = true;
            }
            else
            {
                $green_player = $this->getPlayerByColor(PLAYER_COLOR_GREEN);

                if ($this->wasGreen8PlayedInRound() && $player_id == $green_player['player_id'])
                {
                    // If both Green 8 and Red 7 happen in the same round, the green player discards 2 cards at end of round instead of 1.
                    $num_cards_to_discard = 2;
                }
            }

            // Create an array for each player since their number of cards to discard can be different.
            $args[$player_id] = array(
                'num_cards' => $num_cards_to_discard);
        }

        $args['is_initial_setup'] = $is_initial_setup;
        return $args;
    }

    function argPlayerReclaimDiscardedCard()
    {
        $discarded_cards = $this->cards->getCardsInLocation("discard", $this->getActivePlayerId());
        
        return array('_private' => array('active' => array( // all data inside this array is private; only the active player will see it.
            'discarded_cards' => $discarded_cards)));
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Game state actions
////////////

    /*
        Here, you can create methods defined as "game state actions" (see "action" property in states.inc.php).
        The action method of state X is called everytime the current game state is set to X.
    */
    function stGameStartRound()
    {
        $players = self::loadPlayersBasicInfos();
        $round_number = $this->getRoundNumber();
        $reclaimed_cards = array();
        $destroyed_cards = array();

        foreach ($players as $player_id => $player)
        {
            if ($round_number == 1)
            {
                // In the first round each player starts with a full hand.
                $this->cards->moveAllCardsInLocation("deck", "hand", $player_id, $player_id);
            }
            else
            {
                // If there was a final duel, destroy those cards from the play area.
                $final_duel_slot_num = $this->getNumCardSlots() - $round_number + 2;
                $destroyed_cards[$player_id] = $this->cards->getCardsInLocation("play_area_" . $player_id, $final_duel_slot_num);
                $this->cards->moveAllCardsInLocation("play_area_" . $player_id, "discard", $final_duel_slot_num, $player_id);
                
                // Move all other played cards back to the hand.
                $reclaimed_cards[$player_id] = $this->cards->getCardsInLocation("play_area_" . $player_id);
                $this->cards->moveAllCardsInLocation("play_area_" . $player_id, "hand", null, $player_id);

                $this->resetPlayerGold($player_id);
            }
        }

        // Destroy any unclaimed gold on the table.
        $this->removeGoldFromAllSlots();

        self::notifyAllPlayers("roundStarted", clienttranslate('Round ${round_number} begins. ${expected_num_duels} duels are expected in this round.'), array(
            'round_number' => $round_number,
            'expected_num_duels' => $this->getExpectedNumDuelsInCurrentRound(),
            'reclaimed_cards' => $reclaimed_cards,
            'destroyed_cards' => $destroyed_cards,
            'gold_on_table' => $this->getAllGoldOnTable(),
            'gold_in_supply' => $this->getStartingGoldInSupply()
        ));

        $this->resetDuelNumber();

        if ($this->getRoundNumber() == 1)
        {
            $this->gamestate->nextState("initialSetup");
        }
        else
        {
            $this->gamestate->nextState("firstTurn");
        }
    }

    function stMultiDiscardCards()
    {
        $this->gamestate->setAllPlayersMultiactive();
    }

    function stMultiPlayerTurn()
    {
        $this->gamestate->setAllPlayersMultiactive();
    }

    function stGameDuel()
    {
        $current_duel_number = $this->getDuelNumber();
        $chosenCardPlayerPairs = array();
        $green_card_type_id = -1;
        $red_card_type_id = -1;

        $players = self::loadPlayersBasicInfos();
        foreach ($players as $player_id => $player)
        {
            // Get the card this player chose.
            $card_id = $this->getChosenCardId($player_id);
            $card_name = $this->cards->getCard($card_id)['type'];
            $card_type_id = $this->cards->getCard($card_id)['type_arg'];

            $chosenCardPlayerPairs[] = ['card_type_id' => $card_type_id, 'player_id' => $player_id];

            if ($player['player_color'] == PLAYER_COLOR_GREEN)
                $green_card_type_id = $card_type_id;

            if ($player['player_color'] == PLAYER_COLOR_RED)
                $red_card_type_id = $card_type_id;

            $this->cards->insertCardOnExtremePosition($card_id, "play_area_" . $player_id, true); // true = on top

            // Notify all players about the card played.
            self::notifyAllPlayers("cardPlayed", clienttranslate('${player_name} plays ${card_name}'), array(
                'i18n' => array("card_name"),
                'player_id' => $player_id,
                'player_name' => $player['player_name'],
                'card_name' => $card_name,
                'card_id' => $card_id,
                'card_type_id' => $card_type_id,
                'slot_num' => $current_duel_number
            ));

            $this->resetChosenCardId($player_id);
        }

        // Determine the outcome of the duel.
        $duel_result = $this->determineDuelResult($chosenCardPlayerPairs);
        $winning_player_id = $duel_result['winning_player_id'];
        $winning_gold = $duel_result['winning_gold'];
        $duel_is_tied = $duel_result['duel_is_tied'];
        $round_is_forfeit = $duel_result['round_is_forfeit'];

        // Update the duel results table used when generating the end of round summary.
        $this->insertDuelResult($green_card_type_id, $red_card_type_id, $winning_player_id, $winning_gold);

        // Increment duel stats.
        self::incStat(1, STAT_TABLE_DUEL_COUNT);
        self::incStat($duel_is_tied ? 1 : 0, STAT_TABLE_DUEL_TIE_COUNT);

        if ($winning_player_id > 0)
            self::incStat(1, STAT_PLAYER_DUEL_WIN_COUNT, $winning_player_id);

        if ($round_is_forfeit)
        {
            if ($winning_player_id > 0)
            {
                // Announce the winner.
                $this->awardStarToPlayer($winning_player_id);
                
                self::notifyAllPlayers("roundResult", clienttranslate('${player_name} wins the round by forfeit because an illegal card was played.'), array(
                    'winning_player_id' => $winning_player_id,
                    'player_name' => $this->getPlayerName($winning_player_id),
                    'is_round_done' => true
                ));
            }
            else
            {
                // The whole round is a draw.
                self::notifyAllPlayers("roundResult", clienttranslate('Both players played an illegal card. This round is a draw.'), array(
                    'winning_player_id' => 0,
                    'is_round_done' => true
                ));

                self::incStat(1, STAT_TABLE_ROUND_DRAW_COUNT);
            }

            $this->handleEndOfRound(false);
        }
        else
        {
            if ($duel_is_tied)
            {
                $tied_gold = $this->getTiedGoldAmount();
                $this->addGoldToSlot($current_duel_number, $tied_gold);

                // Duel is a draw.
                self::notifyAllPlayers("duelResult", clienttranslate('No winner. ${winning_gold} gold moves to the middle.'), array(
                    'duel_is_tied' => $duel_is_tied,
                    'winning_gold' => $tied_gold,
                    'duel_number' => $current_duel_number
                ));
            }
            else
            {
                // Award gold.
                $this->awardGoldToPlayer($winning_player_id, $winning_gold);
                $this->addGoldToCard($winning_player_id, $current_duel_number, $winning_gold);

                self::notifyAllPlayers("duelResult", clienttranslate('${player_name} wins ${winning_gold} gold!'), array(
                    'duel_is_tied' => $duel_is_tied,
                    'winning_player_id' => $winning_player_id,
                    'player_name' => $this->getPlayerName($winning_player_id),
                    'winning_gold' => $winning_gold,
                    'duel_number' => $current_duel_number
                ));

                self::incStat($winning_gold, STAT_PLAYER_GOLD_TOTAL, $winning_player_id);
            }

            // Evaluate card abilities that can affect the outcome of the round.
            $this->evaluateCardAbility6();
            $this->evaluateCardAbilityGreen7($current_duel_number, $duel_result['winning_player_id']);

            $this->incrementDuelNumber();

            // Check for end of round and advance to the appropriate game-state.
            $end_round_result = $this->checkEndOfRound();
            $end_round_status = $end_round_result['status'];
            $round_is_ending_early = $end_round_result['is_ending_early'];
            $winning_player_id = $end_round_result['winning_player_id'];
            $winning_gold = $end_round_result['winning_gold'];

            if ($end_round_status == END_ROUND_NO)
            {
                $this->setNextState(STATE_MULTI_PLAYER_TURN);
            }
            else if ($end_round_status == END_ROUND_FINAL_DUEL)
            {
                self::notifyAllPlayers("roundResult", clienttranslate('Both players have the same amount of gold. A final duel will end the round.'), array(
                    'is_round_done' => false
                ));

                self::incStat(1, STAT_TABLE_FINAL_DUEL_COUNT);
                
                $this->setNextState(STATE_MULTI_PLAYER_TURN);
            }
            else // the round is over
            {
                if ($round_is_ending_early)
                    self::notifyAllPlayers("roundEndingEarly", clienttranslate('This round is ending early (red 7 ability)'), array());

                if ($end_round_status == END_ROUND_YES_WINNER)
                {
                    // Announce the winner.
                    $this->awardStarToPlayer($winning_player_id);
                    
                    self::notifyAllPlayers("roundResult", clienttranslate('${player_name} wins the round with ${winning_gold} gold!'), array(
                        'winning_player_id' => $winning_player_id,
                        'player_name' => $this->getPlayerName($winning_player_id),
                        'winning_gold' => $winning_gold,
                        'is_round_done' => true
                    ));
                }
                else
                {
                    // The whole round is a draw.
                    self::notifyAllPlayers("roundResult", clienttranslate('Both players have the same amount of gold. This round is a draw.'), array(
                        'winning_player_id' => 0,
                        'is_round_done' => true
                    ));

                    self::incStat(1, STAT_TABLE_ROUND_DRAW_COUNT);
                }

                $this->handleEndOfRound($round_is_ending_early);
            }
        }

        if ($this->wasGreen8PlayedInDuel($current_duel_number))
        {
            // Green 8 allows the player to take back a discarded card.
            $green_player = $this->getPlayerByColor(PLAYER_COLOR_GREEN);
            $this->gamestate->changeActivePlayer($green_player['player_id']);
            $this->gamestate->nextState("reclaimDiscardedCard");
        }
        else
        {
            $this->transitionToNextState();
        }
    }

//////////////////////////////////////////////////////////////////////////////
//////////// Zombie
////////////

    function zombieGetRandomCardIdInLocation($location, $player_id)
    {
        $cards_in_location = $this->cards->getCardsInLocation($location, $player_id);
        $rand_index = array_rand($cards_in_location);
        $rand_card = $cards_in_location[$rand_index];
        return $rand_card['id'];
    }

    function zombieGetRandomCardIdsInLocation($location, $player_id, $num_card_ids_to_get)
    {
        $rand_card_ids = array();

        if ($num_card_ids_to_get == 1)
        {
            $rand_card_ids[] = $this->zombieGetRandomCardIdInLocation($location, $player_id);
        }
        else
        {
            $cards_in_location = $this->cards->getCardsInLocation($location, $player_id);
            $rand_indices = array_rand($cards_in_location, $num_card_ids_to_get);

            $rand_cards = array();
            for ($i = 0; $i < $num_card_ids_to_get; $i++)
            {
                $rand_index = $rand_indices[$i];
                $rand_card = $cards_in_location[$rand_index];
                $rand_cards[] = $rand_card;
            }

            $rand_card_ids = array_map(function($card) { return $card['id']; }, array_values($rand_cards));
        }

        return $rand_card_ids;
    }

    function zombieChooseCardToPlay($player_id)
    {
        $num_cards_in_hand = $this->cards->countCardInLocation("hand", $player_id);

        if ($num_cards_in_hand > 1)
        {
            $possible_card_ids_to_play = $this->zombieGetRandomCardIdsInLocation("hand", $player_id, 2);

            $card_id_to_play = $possible_card_ids_to_play[0];
            $card = $this->cards->getCard($card_id_to_play);
            $card_to_play_type_id = $card['type_arg'];

            if ($this->isPlayerCardValueExactly1HigherThanPreviousDuel($player_id, $card_to_play_type_id))
            {
                // Illegal move. Choose another card.
                $card_id_to_play = $possible_card_ids_to_play[1];
            }
        }
        else
        {
            $card_id_to_play = $this->zombieGetRandomCardIdInLocation("hand", $player_id);
        }

        return $card_id_to_play;
    }

    /*
        zombieTurn:
        
        This method is called each time it is the turn of a player who has quit the game (= "zombie" player).
        You can do whatever you want in order to make sure the turn of this player ends appropriately
        (ex: pass).
        
        Important: your zombie code will be called when the player leaves the game. This action is triggered
        from the main site and propagated to the gameserver from a server, not from a browser.
        As a consequence, there is no current player associated to this action. In your zombieTurn function,
        you must _never_ use getCurrentPlayerId() or getCurrentPlayerName(), otherwise it will fail with a "Not logged" error message. 
    */
    function zombieTurn($state, $active_player_id)
    {
        $state_name = $state['name'];
        
        switch ($state_name)
        {
            // Active states
            case "playerReclaimDiscardedCard":
                $card_id_to_reclaim = $this->zombieGetRandomCardIdInLocation("discard", $active_player_id);
                $this->reclaimDiscardedCard($active_player_id, $card_id_to_reclaim);
                break;
            
            // Multiactive states
            case "multiDiscardCards":
                $num_cards_to_discard = $this->gamestate->state()['args'][$active_player_id]['num_cards'];
                $card_ids_to_discard = $this->zombieGetRandomCardIdsInLocation("hand", $active_player_id, $num_cards_to_discard);
                $this->discardCards($active_player_id, $card_ids_to_discard);
                break;

            case "multiPlayerTurn":
                $card_id_to_play = $this->zombieChooseCardToPlay($active_player_id);
                $this->playCard($active_player_id, $card_id_to_play);
                break;

            default:
                throw new feException("Zombie mode not supported at this game state: " . $state_name);
                break;
        }
    }
    
///////////////////////////////////////////////////////////////////////////////////:
////////// DB upgrade
//////////

    /*
        upgradeTableDb:
        
        You don't have to care about this until your game has been published on BGA.
        Once your game is on BGA, this method is called everytime the system detects a game running with your old
        Database scheme.
        In this case, if you change your Database scheme, you just have to apply the needed changes in order to
        update the game database and allow the game to continue to run with your new version.
    
    */
    
    function upgradeTableDb($from_version)
    {
        // $from_version is the current version of this game database, in numerical form.
        // For example, if the game was running with a release of your game named "140430-1345",
        // $from_version is equal to 1404301345
        
        // Example:
//        if ($from_version <= 1404301345)
//        {
//            // ! important ! Use DBPREFIX_<table_name> for all tables
//
//            $sql = "ALTER TABLE DBPREFIX_xxxxxxx ....";
//            self::applyDbUpgradeToAllDB($sql);
//        }
//        if ($from_version <= 1405061421)
//        {
//            // ! important ! Use DBPREFIX_<table_name> for all tables
//
//            $sql = "CREATE TABLE DBPREFIX_xxxxxxx ....";
//            self::applyDbUpgradeToAllDB($sql);
//        }
//        // Please add your future database scheme changes here
//
//


    }    
}
