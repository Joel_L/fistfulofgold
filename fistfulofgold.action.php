<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * fistfulofgold implementation : © Joel Lieberman <joellie@live.com>
 *
 * This code has been produced on the BGA studio platform for use on https://boardgamearena.com.
 * See http://en.doc.boardgamearena.com/Studio for more information.
 * -----
 * 
 * fistfulofgold.action.php
 *
 * fistfulofgold main action entry point
 *
 *
 * In this file, you are describing all the methods that can be called from your
 * user interface logic (javascript).
 *       
 * If you define a method "myAction" here, then you can call it from your javascript code with:
 * this.ajaxcall("/fistfulofgold/fistfulofgold/myAction.html", ...)
 *
 * function getArg( $argName, $argType, $mandatory=false, $default=NULL, $argTypeDetails=array(), $bCanFail=false )
 *
 *   This method must be used to retrieve the arguments sent with your AJAX query.
 *   You must not use "_GET", "_POST" or equivalent PHP variables to do this, as it is unsafe.
 *
 *   This method uses the following arguments:
 *
 *     argName: the name of the argument to retrieve.
 *     argType: the type of the argument. You should use one of the following:
 *       'AT_int' for an integer
 *       'AT_posint' for a positive integer 
 *       'AT_float' for a float
 *       'AT_bool' for 1/0/true/false
 *       'AT_enum' for an enumeration (argTypeDetails lists the possible values as an array)
 *       'AT_alphanum' for a string with 0-9a-zA-Z_ and space
 *       'AT_numberlist' for a list of numbers separated with "," or ";" (example: 1,4;2,3;-1,2).
 *       'AT_base64' for a base64-encoded string.
 *     mandatory: specify "true" if the argument is mandatory.
 *     default: if mandatory=false, you can specify here a default value in case the argument is not present.
 *     argTypeDetails: see AT_enum above.
 *     bCanFail: if true, specify that it may be possible that the argument won't be of the type specified by argType (and then do not log this as a fatal error in the system, and return a standard exception to the player).
 *
 * function isArg( $argName )
 *
 *   This is a useful method when you only want to check if an argument is present or not present in your AJAX request (and don't care about the value).
 *   It returns "true" or "false" according to whether "argName" has been specified as an argument of the AJAX request or not.
 */
  
  
class action_fistfulofgold extends APP_GameAction
{ 
    // Constructor: please do not modify
   	public function __default()
  	{
  	    if (self::isArg('notifwindow'))
  	    {
            $this->view = "common_notifwindow";
  	        $this->viewArgs['table'] = self::getArg("table", AT_posint, true);
  	    }
  	    else
  	    {
            $this->view = "fistfulofgold_fistfulofgold";
            self::trace("Complete reinitialization of board game");
        }
  	} 
  	
    public function discardCards()
    {
        self::setAjaxMode();

        // Retrieve arguments.
        $card_ids_raw = self::getArg("card_ids", AT_numberlist, true);

        if ($card_ids_raw == "")
            $card_ids = array();
        else
            $card_ids = explode(",", $card_ids_raw);

        // Take action.
        $this->game->actDiscardCards($card_ids);

        self::ajaxResponse();
    }

    public function playCard()
    {
        self::setAjaxMode();     

        // Retrieve arguments
        // Note: these arguments correspond to what has been sent through the javascript "ajaxcall" method
        $card_id = self::getArg("card_id", AT_posint, true);

        // Then, call the appropriate method in your game logic, like "playCard" or "myAction"
        $this->game->actPlayCard($card_id);

        self::ajaxResponse();
    }

    public function reclaimDiscardedCard()
    {
        self::setAjaxMode();     

        // Retrieve arguments.
        $card_id = self::getArg("card_id", AT_posint, true);

        // Take action.
        $this->game->actReclaimDiscardedCard($card_id);

        self::ajaxResponse();
    }
}
  

