<?php

if (!defined("STAT_TABLE_DUEL_COUNT")) // ensure this block is only invoked once, since it is included multiple times
{
    // State IDs (see states.inc.php)
    define("STATE_GAME_SETUP", 1);
    define("STATE_GAME_ROUND_START", 3);
    define("STATE_MULTI_DISCARD_CARDS", 4);
    define("STATE_MULTI_PLAYER_TURN", 5);
    define("STATE_GAME_DUEL", 10);
    define("STATE_PLAYER_RECLAIM_DISCARDED_CARD", 15);
    define("STATE_GAME_END", 99);

    // Player colors
    define("PLAYER_COLOR_GREEN", "008000");
    define("PLAYER_COLOR_RED", "ff0000");

    // Card definitions (see material.inc.php)
    define("NUM_CARDS_PER_PLAYER", 9);

    define("CARD_TYPE_GREEN_1", 1);
    define("CARD_TYPE_GREEN_2", 2);
    define("CARD_TYPE_GREEN_3", 3);
    define("CARD_TYPE_GREEN_4", 4);
    define("CARD_TYPE_GREEN_5", 5);
    define("CARD_TYPE_GREEN_6", 6);
    define("CARD_TYPE_GREEN_7", 7);
    define("CARD_TYPE_GREEN_8", 8);
    define("CARD_TYPE_GREEN_DYNAMITE", 9);

    define("CARD_TYPE_RED_1", 11);
    define("CARD_TYPE_RED_2", 12);
    define("CARD_TYPE_RED_3", 13);
    define("CARD_TYPE_RED_4", 14);
    define("CARD_TYPE_RED_5", 15);
    define("CARD_TYPE_RED_6", 16);
    define("CARD_TYPE_RED_7", 17);
    define("CARD_TYPE_RED_8", 18);
    define("CARD_TYPE_RED_DYNAMITE", 19);

    // Variant IDs (see gameoptions.inc.php)
    define("OPTION_GAME_LENGTH", 100); // note: game variant ID should start at 100 (ie: 100, 101, 102, ...). The maximum is 199.
        define("GAME_LENGTH_SHORT", 1);
        define("GAME_LENGTH_LONG", 2);
    
    // Stats IDs (see stats.inc.php)
    define("STAT_TABLE_DUEL_COUNT", "duel_count");
    define("STAT_TABLE_DUEL_TIE_COUNT", "duel_tie_count");
    define("STAT_TABLE_FINAL_DUEL_COUNT", "final_duel_count");
    define("STAT_TABLE_ROUND_COUNT", "round_count");
    define("STAT_TABLE_ROUND_DRAW_COUNT", "round_draw_count");

    define("STAT_PLAYER_DUEL_WIN_COUNT", "duel_win_count");
    define("STAT_PLAYER_GOLD_TOTAL", "gold_total");
    define("STAT_PLAYER_GOLD_FROM_TIES", "gold_from_ties");
}    
