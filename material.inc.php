<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * fistfulofgold implementation : © Joel Lieberman <joellie@live.com>
 * 
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * material.inc.php
 *
 * fistfulofgold game material description
 *
 * Here, you can describe the material of your game with PHP variables.
 *   
 * This file is loaded in your game logic class constructor, ie these variables
 * are available everywhere in your game logic code.
 *
 */

require_once("modules/fogdefines.inc.php");

$this->card_defs_by_color = array(
    PLAYER_COLOR_GREEN => array(
        CARD_TYPE_GREEN_1,
        CARD_TYPE_GREEN_2,
        CARD_TYPE_GREEN_3,
        CARD_TYPE_GREEN_4,
        CARD_TYPE_GREEN_5,
        CARD_TYPE_GREEN_6,
        CARD_TYPE_GREEN_7,
        CARD_TYPE_GREEN_8,
        CARD_TYPE_GREEN_DYNAMITE),
    
    PLAYER_COLOR_RED => array(
        CARD_TYPE_RED_1,
        CARD_TYPE_RED_2,
        CARD_TYPE_RED_3,
        CARD_TYPE_RED_4,
        CARD_TYPE_RED_5,
        CARD_TYPE_RED_6,
        CARD_TYPE_RED_7,
        CARD_TYPE_RED_8,
        CARD_TYPE_RED_DYNAMITE)
);

$this->card_defs = array(
    CARD_TYPE_GREEN_1 => array(
        "color" => PLAYER_COLOR_GREEN,
        "name" => "1",
        "value" => 1,
        "wins_vs" => array(CARD_TYPE_RED_5, CARD_TYPE_RED_6, CARD_TYPE_RED_7, CARD_TYPE_RED_8),
        "loses_vs" => array(CARD_TYPE_RED_2, CARD_TYPE_RED_3, CARD_TYPE_RED_4, CARD_TYPE_RED_DYNAMITE)),
    CARD_TYPE_GREEN_2 => array(
        "color" => PLAYER_COLOR_GREEN,
        "name" => "2",
        "value" => 2,
        "wins_vs" => array(CARD_TYPE_RED_1, CARD_TYPE_RED_6, CARD_TYPE_RED_7, CARD_TYPE_RED_8),
        "loses_vs" => array(CARD_TYPE_RED_3, CARD_TYPE_RED_4, CARD_TYPE_RED_5, CARD_TYPE_RED_DYNAMITE)),
    CARD_TYPE_GREEN_3 => array(
        "color" => PLAYER_COLOR_GREEN,
        "name" => "3",
        "value" => 3,
        "wins_vs" => array(CARD_TYPE_RED_1, CARD_TYPE_RED_2, CARD_TYPE_RED_7, CARD_TYPE_RED_8),
        "loses_vs" => array(CARD_TYPE_RED_4, CARD_TYPE_RED_5, CARD_TYPE_RED_6, CARD_TYPE_RED_DYNAMITE)),
    CARD_TYPE_GREEN_4 => array(
        "color" => PLAYER_COLOR_GREEN,
        "name" => "4",
        "value" => 4,
        "wins_vs" => array(CARD_TYPE_RED_1, CARD_TYPE_RED_2, CARD_TYPE_RED_3, CARD_TYPE_RED_8),
        "loses_vs" => array(CARD_TYPE_RED_5, CARD_TYPE_RED_6, CARD_TYPE_RED_7, CARD_TYPE_RED_DYNAMITE)),
    CARD_TYPE_GREEN_5 => array(
        "color" => PLAYER_COLOR_GREEN,
        "name" => "5",
        "value" => 5,
        "wins_vs" => array(CARD_TYPE_RED_2, CARD_TYPE_RED_3, CARD_TYPE_RED_4, CARD_TYPE_RED_DYNAMITE),
        "loses_vs" => array(CARD_TYPE_RED_1, CARD_TYPE_RED_6, CARD_TYPE_RED_7, CARD_TYPE_RED_8)),
    CARD_TYPE_GREEN_6 => array(
        "color" => PLAYER_COLOR_GREEN,
        "name" => "6",
        "value" => 6,
        "wins_vs" => array(CARD_TYPE_RED_3, CARD_TYPE_RED_4, CARD_TYPE_RED_5, CARD_TYPE_RED_DYNAMITE),
        "loses_vs" => array(CARD_TYPE_RED_1, CARD_TYPE_RED_2, CARD_TYPE_RED_7, CARD_TYPE_RED_8),
        "text" => clienttranslate("If you're the first to play a 6, take all the gold from ties (past or future) for this round.")),
    CARD_TYPE_GREEN_7 => array(
        "color" => PLAYER_COLOR_GREEN,
        "name" => "7",
        "value" => 7,
        "wins_vs" => array(CARD_TYPE_RED_4, CARD_TYPE_RED_5, CARD_TYPE_RED_6, CARD_TYPE_RED_DYNAMITE),
        "loses_vs" => array(CARD_TYPE_RED_1, CARD_TYPE_RED_2, CARD_TYPE_RED_3, CARD_TYPE_RED_8),
        "text" => clienttranslate("Each duel won in this round will earn +1 gold."),
        "note" => clienttranslate("This is applied starting from the next duel.")),
    CARD_TYPE_GREEN_8 => array(
        "color" => PLAYER_COLOR_GREEN,
        "name" => "8",
        "value" => 8,
        "wins_vs" => array(CARD_TYPE_RED_5, CARD_TYPE_RED_6, CARD_TYPE_RED_7, CARD_TYPE_RED_DYNAMITE),
        "loses_vs" => array(CARD_TYPE_RED_1, CARD_TYPE_RED_2, CARD_TYPE_RED_3, CARD_TYPE_RED_4),
        "text" => clienttranslate("Take back 1 discarded card. Show it to your opponent."),
        "note" => clienttranslate("At the end of this round, discard 2 cards instead of 1.")),
    CARD_TYPE_GREEN_DYNAMITE => array(
        "color" => PLAYER_COLOR_GREEN,
        "name" => clienttranslate("DYNAMITE"),
        "value" => -1,
        "wins_vs" => array(CARD_TYPE_RED_1, CARD_TYPE_RED_2, CARD_TYPE_RED_3, CARD_TYPE_RED_4),
        "loses_vs" => array(CARD_TYPE_RED_5, CARD_TYPE_RED_6, CARD_TYPE_RED_7, CARD_TYPE_RED_8),
        "text" => clienttranslate("The winner of this duel wins 3 gold.")),
    
    CARD_TYPE_RED_1 => array(
        "color" => PLAYER_COLOR_RED,
        "name" => "1",
        "value" => 1,
        "wins_vs" => array(CARD_TYPE_GREEN_5, CARD_TYPE_GREEN_6, CARD_TYPE_GREEN_7, CARD_TYPE_GREEN_8),
        "loses_vs" => array(CARD_TYPE_GREEN_2, CARD_TYPE_GREEN_3, CARD_TYPE_GREEN_4, CARD_TYPE_GREEN_DYNAMITE)),
    CARD_TYPE_RED_2 => array(
        "color" => PLAYER_COLOR_RED,
        "name" => "2",
        "value" => 2,
        "wins_vs" => array(CARD_TYPE_GREEN_1, CARD_TYPE_GREEN_6, CARD_TYPE_GREEN_7, CARD_TYPE_GREEN_8),
        "loses_vs" => array(CARD_TYPE_GREEN_3, CARD_TYPE_GREEN_4, CARD_TYPE_GREEN_5, CARD_TYPE_GREEN_DYNAMITE)),
    CARD_TYPE_RED_3 => array(
        "color" => PLAYER_COLOR_RED,
        "name" => "3",
        "value" => 3,
        "wins_vs" => array(CARD_TYPE_GREEN_1, CARD_TYPE_GREEN_2, CARD_TYPE_GREEN_7, CARD_TYPE_GREEN_8),
        "loses_vs" => array(CARD_TYPE_GREEN_4, CARD_TYPE_GREEN_5, CARD_TYPE_GREEN_6, CARD_TYPE_GREEN_DYNAMITE)),
    CARD_TYPE_RED_4 => array(
        "color" => PLAYER_COLOR_RED,
        "name" => "4",
        "value" => 4,
        "wins_vs" => array(CARD_TYPE_GREEN_1, CARD_TYPE_GREEN_2, CARD_TYPE_GREEN_3, CARD_TYPE_GREEN_8),
        "loses_vs" => array(CARD_TYPE_GREEN_5, CARD_TYPE_GREEN_6, CARD_TYPE_GREEN_7, CARD_TYPE_GREEN_DYNAMITE)),
    CARD_TYPE_RED_5 => array(
        "color" => PLAYER_COLOR_RED,
        "name" => "5",
        "value" => 5,
        "wins_vs" => array(CARD_TYPE_GREEN_2, CARD_TYPE_GREEN_3, CARD_TYPE_GREEN_4, CARD_TYPE_GREEN_DYNAMITE),
        "loses_vs" => array(CARD_TYPE_GREEN_1, CARD_TYPE_GREEN_6, CARD_TYPE_GREEN_7, CARD_TYPE_GREEN_8)),
    CARD_TYPE_RED_6 => array(
        "color" => PLAYER_COLOR_RED,
        "name" => "6",
        "value" => 6,
        "wins_vs" => array(CARD_TYPE_GREEN_3, CARD_TYPE_GREEN_4, CARD_TYPE_GREEN_5, CARD_TYPE_GREEN_DYNAMITE),
        "loses_vs" => array(CARD_TYPE_GREEN_1, CARD_TYPE_GREEN_2, CARD_TYPE_GREEN_7, CARD_TYPE_GREEN_8),
        "text" => clienttranslate("If you're the first to play a 6, take all the gold from ties (past or future) for this round.")),
    CARD_TYPE_RED_7 => array(
        "color" => PLAYER_COLOR_RED,
        "name" => "7",
        "value" => 7,
        "wins_vs" => array(CARD_TYPE_GREEN_4, CARD_TYPE_GREEN_5, CARD_TYPE_GREEN_6, CARD_TYPE_GREEN_DYNAMITE),
        "loses_vs" => array(CARD_TYPE_GREEN_1, CARD_TYPE_GREEN_2, CARD_TYPE_GREEN_3, CARD_TYPE_GREEN_8),
        "text" => clienttranslate("The round ends at the end of this turn, if this is at least the 3rd duel."),
        "note" => clienttranslate("Each player discards 1 card from his hand.")),
    CARD_TYPE_RED_8 => array(
        "color" => PLAYER_COLOR_RED,
        "name" => "8",
        "value" => 8,
        "wins_vs" => array(CARD_TYPE_GREEN_1, CARD_TYPE_GREEN_5, CARD_TYPE_GREEN_6, CARD_TYPE_GREEN_7, CARD_TYPE_GREEN_DYNAMITE),
        "loses_vs" => array(CARD_TYPE_GREEN_2, CARD_TYPE_GREEN_3, CARD_TYPE_GREEN_4),
        "text" => clienttranslate("If the opponent plays his 1 against this card, the duel is cancelled and you win 3 gold.")),
    CARD_TYPE_RED_DYNAMITE => array(
        "color" => PLAYER_COLOR_RED,
        "name" => clienttranslate("DYNAMITE"),
        "value" => -1,
        "wins_vs" => array(CARD_TYPE_GREEN_1, CARD_TYPE_GREEN_2, CARD_TYPE_GREEN_3, CARD_TYPE_GREEN_4),
        "loses_vs" => array(CARD_TYPE_GREEN_5, CARD_TYPE_GREEN_6, CARD_TYPE_GREEN_7, CARD_TYPE_GREEN_8),
        "text" => clienttranslate("The winner of this duel wins 3 gold."))
);





