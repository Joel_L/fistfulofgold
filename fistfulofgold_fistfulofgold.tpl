{OVERALL_GAME_HEADER}

<!-- 
--------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- fistfulofgold implementation : © Joel Lieberman <joellie@live.com>
-- 
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-------
-->

<div id="fistfulofgold_play_area">
    <div id="my_discarded_cards_container" class="whiteblock">
        <h3>{MY_DISCARDED_CARDS_LABEL}</h3>
        <div id="my_discarded_cards"></div>
    </div>

    <div id="play_mat" class="fog_play_mat">
        <div id="round_number_container">
            <span>{ROUND_LABEL}</span><span id="round_number">N</span>
        </div>

        <div id="fog_player_board_{THEIR_PLAYER_ID}" class="fog_player_board">
            <h3 id="fog_player_name_top" class="fog_player_name" style="color:#{THEIR_PLAYER_COLOR}">{THEIR_PLAYER_NAME} </h3>
            
            <div id="fog_card_row_top" class="fog_card_row">
                <!-- BEGIN their_card_slot -->
                <div id="card_slot_{PLAYER_ID}_{SLOT_NUMBER}" class="fog_card_slot" ></div>
                <!-- END their_card_slot -->
                <div id="card_slot_{THEIR_PLAYER_ID}_{FINAL_SLOT_NUMBER}" class="fog_card_slot fog_final_slot" ></div>
            </div>
        </div>

        <div id="fog_gold_row">
            <!-- BEGIN gold_zone -->
            <div id="gold_zone_{SLOT_NUMBER}" class="fog_gold_zone" ></div>
            <!-- END gold_zone -->
            <div id="gold_zone_{FINAL_SLOT_NUMBER}" class="fog_gold_zone fog_final_slot" ></div>
        </div>

        <div id="fog_player_board_{MY_PLAYER_ID}" class="fog_player_board">
            <div id="fog_card_row_bottom" class="fog_card_row">
                <!-- BEGIN my_card_slot -->
                <div id="card_slot_{PLAYER_ID}_{SLOT_NUMBER}" class="fog_card_slot" ></div>
                <!-- END my_card_slot -->
                <div id="card_slot_{MY_PLAYER_ID}_{FINAL_SLOT_NUMBER}" class="fog_card_slot fog_final_slot" ></div>
            </div>

            <h3 id="fog_player_name_bottom" class="fog_player_name" style="color:#{MY_PLAYER_COLOR}">{MY_PLAYER_NAME}</h3>
        </div>
    </div>

    <div id="my_hand_container" class="whiteblock">
        <h3>{MY_HAND_LABEL}</h3>
        <div id="my_hand"></div>
    </div>

    <div id="general_supply">
        <div id="gold_supply_container" class="whiteblock">
            <h3>{GOLD_SUPPLY_LABEL}</h3>
            <div id="gold_supply">
            </div>
        </div>

        <div id="player_aid">
        </div>
    </div>

    <!-- Only used for querying card dimensions from CSS -->
    <div id="dummy_card" class="fog_card"></div>
    <div id="dummy_card_art" class="fog_card_art"></div>
    <div id="dummy_gold_cube" class="fog_gold_cube"></div>
</div>

<script type="text/javascript">

// Javascript HTML templates

let jstpl_player_panel = '\
    <div>\
        <div id="goldicon_${id}" class="fog_resource_icon fog_gold_icon fog_tt_gold"></div><span id="gold_${id}">${gold}</span>\
    </div>';

let jstpl_card_in_play = '\
    <div id="cardinplay_${player_id}_${slot_number}" class="fog_card">\
        <div class="fog_gold_zone_container">\
            <div id="gold_zone_${player_id}_${slot_number}" class="fog_gold_zone"></div>\
        </div>\
    </div>';

let jstpl_card = '\
    <div class="fog_card">\
        <div class="fog_card_art" style="background-position:-${x}px -${y}px">\
            <div class="fog_card_content">\
                <div class="fog_card_text">${text}</div>\
                <div class="fog_card_note">${note}</div>\
            </div>\
        </div>\
    </div>';

let jstpl_card_tooltip = '\
    <div class="fog_card_tooltip">\
        <h2>${name}</h2>\
        <hr/>\
        <div class="fog_card_art" style="background-position: -${x}px -${y}px;">\
            <div class="fog_card_content">\
                <div class="fog_card_text">${text}</div>\
                <div class="fog_card_note">${note}</div>\
            </div>\
        </div>\
        <hr style="display:${display_style_for_card_text_paragraph_separator};"/>\
        <p class="fog_card_tooltip_card_text">${text}</p>\
        <p class="fog_card_tooltip_card_text">${note}</p>\
        <hr/>\
        <table class="fog_card_tooltip_versus_table">\
            <tr><td>${wins_vs_label}</td><td class="fog_card_tooltip_versus_text">${wins_vs}</td></tr>\
            <tr><td>${loses_vs_label}</td><td class="fog_card_tooltip_versus_text">${loses_vs}</td></tr>\
        </table>\
    </div>';

let jstpl_player_aid_tooltip = '\
    <div class="fog_player_aid_tooltip">\
        <h3>${title}</h3>\
        <p>${subtitle}</p>\
        <hr/>\
        <table class="fog_player_aid_tooltip_table">\
            <tr><td>${table_text_line_1}</td></tr>\
            <tr><td>${table_text_line_2}</td></tr>\
            <tr><td>${table_text_line_3}</td></tr>\
        </table>\
        <hr/>\
        <p class="fog_player_aid_tooltip_text">${body_text_line_1}</p>\
        <p class="fog_player_aid_tooltip_text">${body_text_line_2}</p>\
        <p class="fog_player_aid_tooltip_text">${body_text_line_3}</p>\
    </div>';

let jstpl_gold_cube = '\
    <div class="fog_gold_cube">\
        <div class="fog_gold_cube_art">\
        </div>\
    </div>';

</script>  

{OVERALL_GAME_FOOTER}
