/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * fistfulofgold implementation : © Joel Lieberman <joellie@live.com>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * fistfulofgold.js
 *
 * fistfulofgold user interface script
 * 
 * In this file, you are describing the logic of your user interface, in Javascript language.
 *
 */

define([
    "dojo","dojo/_base/declare",
    "ebg/core/gamegui",
    "ebg/counter",
    "ebg/stock"
],

function (dojo, declare)
{
    // Constants for Stock component's selection modes.
    var STOCK_SELECTION_MODE_OFF = 0;
    var STOCK_SELECTION_MODE_ONE_ITEM = 1;
    var STOCK_SELECTION_MODE_MULTIPLE_ITEMS = 2;

    return declare("bgagame.fistfulofgold", ebg.core.gamegui,
    {
        constructor: function()
        {
            // Global variable initialization
            this.playerHand = null;
            this.playerDiscard = null;
            this.playerChosenCardId = 0;
            this.previousPlayedCardTypeId = 0;
            this.goldZones = {};
            this.goldSupply = null;
            this.colorGreen = "008000";
            this.colorRed = "ff0000";
        },
        
        /*
            setup:
            
            This method must set up the game user interface according to current game situation specified
            in parameters.
            
            The method is called each time the game interface is displayed to a player, ie:
            _ when the game starts
            _ when a player refreshes the game page (F5)
            
            "gamedatas" argument contains all datas retrieved by your "getAllDatas" PHP method.
        */
        
        setup: function(gamedatas)
        {
            var top_bar_size = dojo.position("topbar");
            var action_bar_size = dojo.position("page-title");

            var header_height = top_bar_size.h + action_bar_size.h;

            var play_area_width = action_bar_size.w;
            var play_area_height = window.innerHeight - header_height;

            // Determine layout size based on screen width.
            if (play_area_width >= 1550 && play_area_height >= 980) // allow 255 pixels extra width in case the player has player panels displayed in a second column
            {
                dojo.addClass("ebd-body", "fistfulofgold_large");
                this.dontPreloadImage("playmat.jpg");
            }
            else
            {
                this.dontPreloadImage("playmat_1280.jpg");
            }

            // Update the round number display.
            this.setRoundNumber(gamedatas.round_number);

            // Update the style of active slots in this round so they are easily identified.
            this.updateActiveSlots(gamedatas.expected_num_duels);

            // Read the computed card dimensions from CSS now that the layout size has been determined.
            var dummy_card_element = dojo.byId("dummy_card");
            var cardWidth = dojo.getStyle(dummy_card_element, "width");
            var cardHeight = dojo.getStyle(dummy_card_element, "height");
    
            // My hand
            this.playerHand = new ebg.stock();
            this.playerHand.create(this, $("my_hand"), cardWidth, cardHeight);
            this.playerHand.image_items_per_row = 10;
            this.playerHand.item_margin = 2;
            this.playerHand.centerItems = true;
            this.playerHand.onItemCreate = dojo.hitch(this, "setupNewCard"); 
            this.playerHand.setSelectionAppearance("class");
            dojo.connect(this.playerHand, "onChangeSelection", this, "onPlayerHandSelectionChanged");

            // My discarded cards (only used for Green 8 ability)
            this.playerDiscard = new ebg.stock();
            this.playerDiscard.create(this, $("my_discarded_cards"), cardWidth, cardHeight);
            this.playerDiscard.image_items_per_row = 10;
            this.playerDiscard.item_margin = 2;
            this.playerDiscard.centerItems = true;
            this.playerDiscard.onItemCreate = dojo.hitch(this, "setupNewCard"); 
            dojo.connect(this.playerDiscard, "onChangeSelection", this, "onPlayerDiscardSelectionChanged");

            // Create card types
            for (var card_type_id in gamedatas.card_defs)
            {
                // Note: Not using Stock's built-in sprite handling; using onItemCreate (above) instead and placing sprites in CSS.
                // This way the handling is the same for cards in the hand or discard, and cards placed on the table.
                this.playerHand.addItemType(card_type_id, card_type_id, "", 0);
                this.playerDiscard.addItemType(card_type_id, card_type_id, "", 0);
            }

            // Populate hand
            for (var hand_card_id in gamedatas.my_hand)
            {
                var hand_card = gamedatas.my_hand[hand_card_id];
                var type_id = hand_card.type_arg;

                this.playerHand.addToStockWithId(type_id, hand_card.id);
            }

            for (var player_id in gamedatas.players)
            {
                var player = gamedatas.players[player_id];

                // Cards played on table
                for (var table_card_id in gamedatas.cards_in_play[player.id])
                {
                    var table_card = gamedatas.cards_in_play[player.id][table_card_id];
                    var num_gold_cubes_on_card = gamedatas.gold_on_table[table_card.location_arg][player.id].gold;

                    this.placeCardOnTable(player.id, table_card.id, table_card.type_arg, table_card.location_arg, num_gold_cubes_on_card);

                    // Update this player's previously played card for validating card selection (see validatePlayCard).
                    if (player.id == this.player_id && table_card.location_arg == this.gamedatas.duel_number - 1)
                        this.previousPlayedCardTypeId = table_card.type_arg;
                }

                // Player panels
                var player_panel_div = $(this.getPlayerPanelElementId(player_id));
                dojo.place(this.format_block("jstpl_player_panel", player), player_panel_div);

                // Remember the local player's chosen card so the selection highlight can be re-applied on page refresh (see onEnteringState).
                if (player_id == this.player_id)
                    this.playerChosenCardId = player.chosen_card_id;
            }

            // Gold on the table from tied duels.
            var tied_zone_index = 0;
            for (var slot_num in gamedatas.gold_on_table)
            {
                var num_gold_cubes = gamedatas.gold_on_table[slot_num][tied_zone_index].gold;
                this.createGoldZone(tied_zone_index, slot_num, num_gold_cubes);
            }

            // Gold in the general supply.
            this.createGoldSupply(gamedatas.gold_in_supply);

            // Tooltips
            this.addTooltipToClass('fog_tt_gold', _('Gold won in this round'), '');

            var tooltipHtml = this.format_block("jstpl_player_aid_tooltip",
            {
                title: _("REMINDER"),
                subtitle: _("9 cards in hand. 2 are discarded."),
                table_text_line_1: _("ROUND 1: 7 cards in hand; 6 are played; the last one is discarded"),
                table_text_line_2: _("ROUND 2: 6 cards in hand; 5 are played; the last one is discarded"),
                table_text_line_3: _("ROUND 3: 5 cards in hand; 4 are played"),
                body_text_line_1: _("At each duel, the winner wins as much gold as the difference between the 2 cards."),
                body_text_line_2: _("Winner of the round: the player who won the most gold."),
                body_text_line_3: _("The game is won in 2 winning rounds.")
            });
        
            this.addTooltipHtml("player_aid", tooltipHtml, 100);
            
            // Setup game notifications to handle (see "setupNotifications" method below)
            this.setupNotifications();
        },


        ///////////////////////////////////////////////////
        //// Game & client states
        
        // onEnteringState: this method is called each time we are entering into a new game state.
        //                  You can use this method to perform some user interface changes at this moment.
        //
        onEnteringState: function(stateName, args)
        {
            switch (stateName)
            {            
            case "multiDiscardCards":
                // Override the description for this state since it can be different for each player.
                this.playerHand.setSelectionMode(STOCK_SELECTION_MODE_MULTIPLE_ITEMS);
                break;

            case "multiPlayerTurn":
                this.playerHand.setSelectionMode(STOCK_SELECTION_MODE_ONE_ITEM);

                // If the player refreshes the page after selecting a card, re-apply the selection highlight here so they know what they chose.
                if (this.playerChosenCardId > 0)
                    this.playerHand.selectItem(this.playerChosenCardId);
                break;

            case "playerReclaimDiscardedCard":
                if (this.isCurrentPlayerActive())
                {    
                    // Show the discarded cards.
                    dojo.style("my_discarded_cards_container", "display", "block");

                    for (var card_id in args.args._private.discarded_cards)
                    {
                        var card = args.args._private.discarded_cards[card_id];
                        var type_id = card.type_arg;
        
                        this.playerDiscard.addToStockWithId(type_id, card.id);
                    }
                }
                break;
            }

            this.updateDescriptionForGameState(stateName, args);
        },

        // onLeavingState: this method is called each time we are leaving a game state.
        //                 You can use this method to perform some user interface changes at this moment.
        //
        onLeavingState: function(stateName)
        {
            switch (stateName)
            {
            case "multiPlayerTurn":
                this.playerHand.setSelectionMode(STOCK_SELECTION_MODE_OFF);
                this.playerChosenCardId = 0;
                break;

            case "playerReclaimDiscardedCard":
                // Hide the discarded cards.
                dojo.style("my_discarded_cards_container", "display", "none");
                break;           
            }               
        }, 

        // onUpdateActionButtons: in this method you can manage "action buttons" that are displayed in the
        //                        action status bar (ie: the HTML links in the status bar).
        //        
        onUpdateActionButtons: function(stateName, args)
        {
            if (this.isCurrentPlayerActive())
            {            
                switch (stateName)
                {
                    case "multiDiscardCards":
                        var num_cards = args[this.player_id].num_cards;
                        this.addActionButton("button_confirm_discard_" + num_cards, _("Confirm"), "onConfirmDiscard");
                        break;
                }
            }
        },        

        ///////////////////////////////////////////////////
        //// Utility methods
        
        /*
        
            Here, you can defines some utility methods that you can use everywhere in your javascript
            script.
        
        */
        setRoundNumber: function(round_number)
        {
            var round_number_element = $("round_number");
            round_number_element.innerHTML = round_number;
        },

        updateActiveSlots: function(num_active_slots)
        {
            dojo.query(".fog_card_slot").removeClass("fog_card_slot_active");    
            for (var player_id in this.gamedatas.players)
            {
                for (var slot_num = 1; slot_num <= num_active_slots; slot_num++)
                {
                    var card_slot_element_id = "card_slot_" + player_id + "_" + slot_num;                    
                    dojo.addClass(card_slot_element_id, "fog_card_slot_active");
                }
            }
        },

        getPlayerColor: function(player_id)
        {
            return this.gamedatas.players[player_id].color;
        },

        getPlayerColoredYouHtml: function()
        {
            var color = this.gamedatas.players[this.player_id].color;
            var color_bg = "";
            if (this.gamedatas.players[this.player_id] && this.gamedatas.players[this.player_id].color_back) {
                color_bg = "background-color:#" + this.gamedatas.players[this.player_id].color_back + ";";
            }
            var you = "<span style=\"font-weight:bold;color:#" + color + ";" + color_bg + "\">" + __("lang_mainsite", "You") + "</span>";
            return you;            
        },

        getPlayerPanelElementId: function(player_id)
        {
            return "player_board_" + player_id;
        },

        getCardSpriteOffset: function(card_type_id)
        {
            var card = this.gamedatas.card_defs[card_type_id];

            // Read the computed card art dimensions from CSS.
            var dummy_card_art_element = dojo.byId("dummy_card_art");
            var card_sprite_width = dojo.getStyle(dummy_card_art_element, "width");
            var card_sprite_height = dojo.getStyle(dummy_card_art_element, "height");

            var card_sprite_x_offset = card_sprite_width * (card_type_id - 1 - (card.color == this.colorRed ? 10 : 0));
            var card_sprite_y_offset = card_sprite_height * (card.color == this.colorRed ? 1 : 0); // red cards are on the second row in the sprite-sheet

            return {
                x: card_sprite_x_offset,
                y: card_sprite_y_offset
            }
        },

        setupNewCard: function(card_div, card_type_id, card_id)
        {
            var card = this.gamedatas.card_defs[card_type_id];            
            var card_sprite_offset = this.getCardSpriteOffset(card_type_id);

            if (typeof card.text == "undefined")
                card.text = "";

            if (typeof card.note == "undefined")
                card.note = "";

            card.text = this.nl2br(_(card.text), false);
            card.note = this.nl2br(_(card.note), false);

            var has_card_text = true;
            if (card.text.length == 0 && card.note.length == 0)
                has_card_text = false;

            var that = this;
            var tooltipHtml = this.format_block("jstpl_card_tooltip",
            {
                name: card.name,
                x: card_sprite_offset.x,
                y: card_sprite_offset.y,
                display_style_for_card_text_paragraph_separator: has_card_text ? "inherit" : "none",
                text: card.text,
                note: card.note,
                wins_vs_label: _("Wins against:"),
                wins_vs: card.wins_vs.map(function(type_id) { return that.gamedatas.card_defs[type_id].name }),
                loses_vs_label: _("Loses against:"),
                loses_vs: card.loses_vs.map(function(type_id) { return that.gamedatas.card_defs[type_id].name })
            });
        
            this.addTooltipHtml(card_div.id, tooltipHtml, 100);

            dojo.place(
                this.format_block("jstpl_card",
                {
                    id: card_id,
                    x: card_sprite_offset.x,
                    y: card_sprite_offset.y,
                    text: card.text,
                    note: card.note
                }), card_div.id);
        },

        getGoldCubeSize: function()
        {
            // Read the gold cube dimensions from CSS.
            var dummy_gold_cube_element = dojo.byId("dummy_gold_cube");
            var gold_cube_sprite_width = dojo.getStyle(dummy_gold_cube_element, "width");
            var gold_cube_sprite_height = dojo.getStyle(dummy_gold_cube_element, "height");

            return {
                width: gold_cube_sprite_width,
                height: gold_cube_sprite_height
            }
        },

        createGoldZone: function(player_id, slot_num, num_gold_cubes_to_add)
        {
            var gold_cube_size = this.getGoldCubeSize();
    
            // If no player ID is associated with the gold, it goes in the middle of the table between the two played cards.
            var zone_element_id = "gold_zone_" + slot_num;
            if (player_id > 0)
                zone_element_id = "gold_zone_" + player_id + "_" + slot_num;

            if (!this.goldZones[player_id])
                this.goldZones[player_id] = new Array();

            this.goldZones[player_id][slot_num] = new ebg.stock();
            this.goldZones[player_id][slot_num].create(this, $(zone_element_id), gold_cube_size.width, gold_cube_size.height);
            this.goldZones[player_id][slot_num].addItemType(0, 0, "")
            this.goldZones[player_id][slot_num].item_margin = 10;
            this.goldZones[player_id][slot_num].centerItems = true;
            this.goldZones[player_id][slot_num].onItemCreate = dojo.hitch(this, "createGoldCube");
            this.goldZones[player_id][slot_num].setSelectionMode(STOCK_SELECTION_MODE_OFF);

            for (var i = 0; i < num_gold_cubes_to_add; i++)
                this.goldZones[player_id][slot_num].addToStock(0);
        },

        createGoldSupply: function(num_gold_cubes_to_add)
        {
            var gold_cube_size = this.getGoldCubeSize();
    
            // If no player ID is associated with the gold, it goes in the middle of the table between the two played cards.
            var zone_element_id = "gold_supply";

            this.goldSupply = new ebg.stock();
            this.goldSupply.create(this, $(zone_element_id), gold_cube_size.width, gold_cube_size.height);
            this.goldSupply.addItemType(0, 0, "")
            this.goldSupply.item_margin = 4;
            this.goldSupply.centerItems = false;
            this.goldSupply.onItemCreate = dojo.hitch(this, "createGoldCube");
            this.goldSupply.setSelectionMode(STOCK_SELECTION_MODE_OFF);

            for (var i = 0; i < num_gold_cubes_to_add; i++)
                this.goldSupply.addToStock(0);
        },

        createGoldCube: function(parent_div/*, item_type_id, item_id*/)
        {
            dojo.place(this.format_block("jstpl_gold_cube", {}), parent_div);
        },

        discardCards: function(card_ids)
        {
            for (var i = 0; i < card_ids.length; i++)
            {
                var card_id = card_ids[i];
                this.playerHand.removeFromStockById(card_id);
            }
        },

        updateDescriptionForGameState: function(stateName, args)
        {
            if (!this.isSpectator)
            {
                switch (stateName)
                {
                    case "multiDiscardCards":
                        this.gamedatas.gamestate.descriptionmyturn = dojo.string.substitute(_("${you} must choose ${n} cards to discard"), {
                            n: args.args[this.player_id].num_cards,
                            you: this.getPlayerColoredYouHtml()
                        });
                        break;

                    case "multiPlayerTurn":
                        if (this.playerChosenCardId > 0)
                            this.gamedatas.gamestate.description = dojo.string.substitute(_("You may change your choice while waiting for other players"), { });    
                        break;
                }

                this.updatePageTitle();
            }
        },

        validatePlayCard: function(card_type_id)
        {
            if (this.previousPlayedCardTypeId > 0)
            {
                var card_value = this.gamedatas.card_defs[card_type_id].value;    
                var previous_card_value = this.gamedatas.card_defs[this.previousPlayedCardTypeId].value;
                var num_cards_in_hand = this.playerHand.count();
    
                if (card_value == previous_card_value + 1 && num_cards_in_hand > 1)
                    return false;
            }

            return true;
        },

        placeCardOnTable: function(player_id, card_id, card_type_id, slot_num, num_gold_cubes_on_card)
        {
            var card_slot_element_id = "card_slot_" + player_id + "_" + slot_num;
            var card_in_play_element_id = "cardinplay_" + player_id + "_" + slot_num;

            dojo.place(
                this.format_block("jstpl_card_in_play",
                {
                    player_id: player_id,
                    slot_number: slot_num
                }), card_slot_element_id);

            this.createGoldZone(player_id, slot_num, num_gold_cubes_on_card);

            this.setupNewCard($(card_in_play_element_id), card_type_id, card_id);
                
            if (player_id != this.player_id)
            {
                // An opponent played a card. Place it on their player panel.
                this.placeOnObject(card_in_play_element_id, this.getPlayerPanelElementId(player_id));
            }
            else
            {
                // Active player played a card. Remove it from their hand and place it in front of them.
                if ($(this.playerHand.getItemDivId(card_id)))
                {
                    this.placeOnObject(card_in_play_element_id, this.playerHand.getItemDivId(card_id));
                    this.playerHand.removeFromStockById(card_id);
                }
            }

            // In either case: animate it to its final destination
            this.slideToObject(card_in_play_element_id, card_slot_element_id).play();

            // Remove the active style from the slot where the card was played.
            dojo.removeClass(card_slot_element_id, "fog_card_slot_active");

            return card_in_play_element_id;
        },

        awardGoldToPlayer: function(player_id, gold, from_slot_num, to_slot_num)
        {
            var i = 0;
            var from_element_id = "gold_supply";

            if (from_slot_num != null)
            {
                from_element_id = "gold_zone_" + from_slot_num;
            }

            var that = this;
            setTimeout(function() {
                // Add the gold to the appropriate card on the winning player's side.
                for (var gold_count = 0; gold_count < gold; gold_count++)
                    that.goldZones[player_id][to_slot_num].addToStock(0, from_element_id);

                if (from_slot_num == null)
                {
                    // If no source is specified the gold comes form the general supply.
                    for (i = 0; i < gold; i++)
                        that.goldSupply.removeFromStock(0);
                }            
                else
                {
                    // Otherwise it comes from the tied gold between the two cards.
                    for (i = 0; i < gold; i++)
                        that.goldZones[0][from_slot_num].removeFromStock(0); // true => don't update the zone display yet; this causes a glitch in the animation of the remaining items
                }
                
                // Update the gold amount in the player panel.
                var gold_element = $("gold_" + player_id);
                gold_element.innerHTML = parseInt(gold_element.innerHTML, 10) + gold;
            }, 500);
        },

        resetPlayerGold: function(player_id)
        {
            var gold_element = $("gold_" + player_id);
            gold_element.innerHTML = 0;
        },

        ///////////////////////////////////////////////////
        //// Player's action
        
        /*
        
            Here, you are defining methods to handle player's action (ex: results of mouse click on 
            game objects).
            
            Most of the time, these methods:
            _ check the action is possible at this game state.
            _ make a call to the game server
        
        */
        onConfirmDiscard: function(evt)
        {
            // Extract number of cards from Confirm button ID.            
            var num_cards = evt.currentTarget.id.split('_')[3]; // ID format: button_confirm_discard_<num>

            // Get the cards.
            var items = this.playerHand.getSelectedItems();
            var card_ids = [];

            if (items.length == num_cards)
            {
                if (this.checkAction("discardCards"))
                {
                    card_ids = items.map(function(item) { return item.id; });
                    
                    this.ajaxcall("/fistfulofgold/fistfulofgold/discardCards.html", { 
                        card_ids: card_ids.join(),
                        lock: true 
                        }, this, function(/*result*/) { }, function(/*is_error*/) { });
                    
                    this.playerHand.setSelectionMode(STOCK_SELECTION_MODE_OFF);
                }
            }
            else
            {
                this.showMessage(dojo.string.substitute(_("You must discard exactly ${n} cards"), { n: num_cards }), "error");
            }
        },

        onPlayerHandSelectionChanged: function(/*control_name, item_id*/)
        {
            var items = this.playerHand.getSelectedItems();

            if (items.length > 0)
            {
                if (this.checkPossibleActions("playCard", true)) // checkPossibleActions doesn't check if the player is active, allowing them to change their mind about the chosen card
                {
                    var card_id = items[0].id;
                    var card_type_id = items[0].type;

                    if (this.validatePlayCard(card_type_id))
                    {
                        this.playerChosenCardId = card_id;

                        this.ajaxcall("/fistfulofgold/fistfulofgold/playCard.html", { 
                                card_id: card_id,
                                lock: true 
                            }, this,
                            function(/*result*/) { },
                            function(is_error)
                            {
                                if (!is_error)
                                    this.updateDescriptionForGameState("multiPlayerTurn");
                            });    
                    }
                    else
                    {
                        this.showMessage(_("You can't play a card with a value 1 higher than the previous duel"), "error");
    
                        // If there's an error, reset the player's card choice to the one they had previously selected.
                        this.playerHand.unselectAll();
                        if (this.playerChosenCardId > 0)
                            this.playerHand.selectItem(this.playerChosenCardId);
                    }
                }
            }
        },

        onPlayerDiscardSelectionChanged: function(/*control_name, item_id*/)
        {
            var items = this.playerDiscard.getSelectedItems();

            if (items.length > 0)
            {
                if (this.checkAction("reclaimDiscardedCard", true)) // true => don't show an error message
                {
                    var card_id = items[0].id;

                    this.ajaxcall("/fistfulofgold/fistfulofgold/reclaimDiscardedCard.html", { 
                        card_id: card_id,
                        lock: true 
                        }, this, function(/*result*/) { }, function(/*is_error*/) { });

                    this.playerDiscard.unselectAll();
                }
            }
        },

       
        ///////////////////////////////////////////////////
        //// Reaction to cometD notifications

        /*
            setupNotifications:
            
            In this method, you associate each of your game notifications with your local method to handle it.
            
            Note: game notification names correspond to "notifyAllPlayers" and "notifyPlayer" calls in
                  your fistfulofgold.game.php file.
        
        */
        setupNotifications: function()
        {
            // Public (all players) notifications
            dojo.subscribe("roundStarted", this, "notif_roundStarted");
            dojo.subscribe("cardPlayed", this, "notif_cardPlayed");
            dojo.subscribe("cardReclaimed", this, "notif_cardReclaimed");   this.notifqueue.setSynchronous("cardReclaimed", 3000);
            dojo.subscribe("duelResult", this, "notif_duelResult");         this.notifqueue.setSynchronous("duelResult", 1000);
            dojo.subscribe("roundResult", this, "notif_roundResult");       this.notifqueue.setSynchronous("roundResult", 2000);
            dojo.subscribe("tableWindow", this, "notif_tableWindow");

            // Card abilities
            dojo.subscribe("ability6", this, "notif_ability6");             this.notifqueue.setSynchronous("ability6", 1000);
            dojo.subscribe("abilityGreen7", this, "notif_abilityGreen7");   this.notifqueue.setSynchronous("abilityGreen7", 1000);

            // Private (only this player) notifications
            dojo.subscribe("cardsDiscarded_Private", this, "notif_cardsDiscarded_Private");
        },

        notif_cardsDiscarded_Private: function(notif)
        {
            this.discardCards(notif.args.card_ids);
        },

        notif_roundStarted: function(notif)
        {
            // Update the round number display.
            this.setRoundNumber(notif.args.round_number);

            // Update the style of active slots in this round so they are easily identified.
            this.updateActiveSlots(notif.args.expected_num_duels);

            // Reset the duel number used for validating card selection (see validatePlayCard).
            this.previousPlayedCardTypeId = 0;
            
            // These settings ensure the cards fade or slide progressively to their destination instead of all at once.
            var slide_duration = 500; // ms
            var slide_delay = 0; // ms
            var slide_increment = 200; // ms

            // Destroy any unclaimed gold on the table.
            for (var slot_num in this.gamedatas.gold_on_table)
                this.goldZones[0][slot_num].removeAllTo("gold_supply");

            // Refill the gold supply.
            this.goldSupply.removeAll();
            for (var i = 0; i < notif.args.gold_in_supply; i++)
                this.goldSupply.addToStock(0);

            // Destroy any cards used in the final duel
            for (var destroyer_player_id in notif.args.destroyed_cards)
            {
                var destroyed_cards = notif.args.destroyed_cards[destroyer_player_id];

                for (var destroyed_card_id in destroyed_cards)
                {
                    var destroyed_card = destroyed_cards[destroyed_card_id];    
                    var destroyed_card_element_id = "cardinplay_" + destroyer_player_id + "_" + destroyed_card.location_arg;

                    var fade_duration = 1000;
                    slide_delay = fade_duration;
                    this.fadeOutAndDestroy(destroyed_card_element_id, fade_duration);
                }
            }

            // Re-populate players hands
            for (var reclaimer_player_id in notif.args.reclaimed_cards)
            {
                var reclaimed_cards = notif.args.reclaimed_cards[reclaimer_player_id];

                for (var reclaimed_card_id in reclaimed_cards)
                {
                    var reclaimed_card = reclaimed_cards[reclaimed_card_id];
                    var reclaimed_card_type_id = reclaimed_card.type_arg;    
                    var reclaimed_card_element_id = "cardinplay_" + reclaimer_player_id + "_" + reclaimed_card.location_arg;
    
                    if (reclaimer_player_id == this.player_id)
                    {
                        this.playerHand.addToStockWithId(reclaimed_card_type_id, reclaimed_card.id, reclaimed_card_element_id);
                        dojo.destroy(reclaimed_card_element_id);
                    }
                    else
                    {
                        this.slideToObjectAndDestroy(reclaimed_card_element_id, this.getPlayerPanelElementId(reclaimer_player_id), slide_duration, slide_delay);
                        slide_delay += slide_increment;
                    }
                }
            }
        },

        notif_cardPlayed: function(notif)
        {
            // Play a card on the table
            this.placeCardOnTable(notif.args.player_id, notif.args.card_id, notif.args.card_type_id, notif.args.slot_num, 0);

            if (notif.args.player_id == this.player_id)
                this.previousPlayedCardTypeId = notif.args.card_type_id;
        },

        notif_cardReclaimed: function(notif)
        {
            if (notif.args.player_id == this.player_id)
            {
                // Move the card from discard to hand.
                this.playerHand.addToStockWithId(notif.args.card_type_id, notif.args.card_id, this.playerDiscard.getItemDivId(notif.args.card_id));
                this.playerDiscard.removeFromStockById(notif.args.card_id);
            }
            else
            {
                // An opponent took back a card. Reveal it in the middle of the playmat.
                var card_in_play_element_id = this.placeCardOnTable(notif.args.player_id, notif.args.card_id, notif.args.card_type_id, notif.args.slot_num, 0)

                // Then slide it to their player panel after a short delay.
                var that = this;
                setTimeout(function() {
                    that.slideToObjectAndDestroy(card_in_play_element_id, that.getPlayerPanelElementId(notif.args.player_id), 500, 2000);
                }, 1000);
            }
        },        

        notif_duelResult: function(notif)
        {
            if (notif.args.duel_is_tied)
            {
                // Duel is tied. Animate gold from the general supply to the middle of the playmat, between the tied cards.
                for (var i = 0; i < notif.args.winning_gold; i++)
                {
                    this.goldZones[0][notif.args.duel_number].addToStock(0, "gold_supply");
                    this.goldSupply.removeFromStock(0);
                }
            }
            else
            {
                // Update the gold value in the player panel and also show the player's winnings as gold cubes on their card.
                this.awardGoldToPlayer(notif.args.winning_player_id, notif.args.winning_gold, null, notif.args.duel_number);
            }
        },

        notif_roundResult: function(notif)
        {
            if (notif.args.is_round_done)
            {
                for (var player_id in this.gamedatas.players)
                {
                    // Clear players gold value.
                    this.resetPlayerGold(player_id);
                }

                // Increment star count for the winning player (if there is one).
                if (notif.args.winning_player_id > 0)
                {
                    this.scoreCtrl[notif.args.winning_player_id].incValue(1);
                }
                else
                {
                    // The players are still tied after the final duel. The round is a draw.
                }
            }
            else
            {
                // The players are tied on gold. A final duel will happen to break the tie.
            }
        },

        // This is the scoring dialog that summarizes the round results.
        notif_tableWindow: function(/*notif*/)
        {
            // Add some styling to the results table.
            dojo.query(".tableWindow table").addClass("fog_scoring_dialog_table");
            dojo.query(".tableWindow tr").addClass("fog_scoring_dialog_row");
        },

        notif_ability6: function(notif)
        {
            this.awardGoldToPlayer(notif.args.player_id, notif.args.gold, notif.args.slot_num, notif.args.slot_num);
        },

        notif_abilityGreen7: function(notif)
        {
            this.awardGoldToPlayer(notif.args.player_id, notif.args.gold, null, notif.args.duel_number);
        }
   });             
});
