
-- ------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- fistfulofgold implementation : © Joel Lieberman <joellie@live.com>
-- 
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-- -----

-- dbmodel.sql

-- This is the file where you are describing the database schema of your game
-- Basically, you just have to export from PhpMyAdmin your table structure and copy/paste
-- this export here.
-- Note that the database itself and the standard tables ("global", "stats", "gamelog" and "player") are
-- already created and must not be created here

-- Note: The database schema is created from this file when the game starts. If you modify this file,
--       you have to restart a game to see your changes in database.

CREATE TABLE IF NOT EXISTS `card` (
  `card_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `card_type` VARCHAR(16) NOT NULL,
  `card_type_arg` INT NOT NULL,
  `card_location` VARCHAR(20) NOT NULL,
  `card_location_arg`INT NOT NULL,
  PRIMARY KEY (`card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `gold_zone` (
  `player_id` INT NOT NULL DEFAULT 0,
  `slot_num` INT UNSIGNED NOT NULL,
  `gold` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`player_id`, `slot_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `duel` (
  `duel_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `round_num` INT UNSIGNED NOT NULL,
  `duel_num` INT UNSIGNED NOT NULL,
  `green_card_type_id` INT NOT NULL,
  `red_card_type_id` INT NOT NULL,
  `winning_player_id` INT NOT NULL DEFAULT 0,
  `winning_gold` INT UNSIGNED NOT NULL DEFAULT 0,
  `bonus_player_id` INT NOT NULL DEFAULT 0,
  `bonus_gold` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`duel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `player`
  ADD `player_gold` INT(10) UNSIGNED NOT NULL DEFAULT 0,
  ADD `player_chosen_card_id` INT(10) UNSIGNED NOT NULL DEFAULT 0;
