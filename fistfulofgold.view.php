<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * fistfulofgold implementation : © Joel Lieberman <joellie@live.com>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * fistfulofgold.view.php
 *
 * This is your "view" file.
 *
 * The method "build_page" below is called each time the game interface is displayed to a player, ie:
 * _ when the game starts
 * _ when a player refreshes the game page (F5)
 *
 * "build_page" method allows you to dynamically modify the HTML generated for the game interface. In
 * particular, you can set here the values of variables elements defined in fistfulofgold_fistfulofgold.tpl (elements
 * like {MY_VARIABLE_ELEMENT}), and insert HTML block elements (also defined in your HTML template file)
 *
 * Note: if the HTML of your game interface is always the same, you don't have to place anything here.
 *
 */
  
  require_once(APP_BASE_PATH . "view/common/game.view.php");
  
  class view_fistfulofgold_fistfulofgold extends game_view
  {
    function getGameName()
    {
        return "fistfulofgold";
    }

  	function build_page($viewArgs)
  	{		
  	    // Get players & players number
        $players = $this->game->loadPlayersBasicInfos();
        $players_nbr = count($players);

        /*********** Place your code below:  ************/


        /*
        
        // Examples: set the value of some element defined in your tpl file like this: {MY_VARIABLE_ELEMENT}

        // Display a specific number / string
        $this->tpl['MY_VARIABLE_ELEMENT'] = $number_to_display;

        // Display a string to be translated in all languages: 
        $this->tpl['MY_VARIABLE_ELEMENT'] = self::_("A string to be translated");

        // Display some HTML content of your own:
        $this->tpl['MY_VARIABLE_ELEMENT'] = self::raw($some_html_code);
        
        */
        
        /*
        
        // Example: display a specific HTML block for each player in this game.
        // (note: the block is defined in your .tpl file like this:
        //      <!-- BEGIN myblock --> 
        //          ... my HTML code ...
        //      <!-- END myblock --> 
        
        */

        global $g_user;
        $current_player_id = $g_user->get_id();
        $isSpectator = !isset($players[$current_player_id]);

        $this->tpl["MY_DISCARDED_CARDS_LABEL"] = self::_("Your discarded cards");
        $this->tpl["MY_HAND_LABEL"] = self::_("Your hand");
        $this->tpl["GOLD_SUPPLY_LABEL"] = self::_("Gold supply");

        foreach ($players as $player)
        {
            $player_num = $player['player_no'];

            $template_variable_prefix = "THEIR_";
            $card_slot_block_name = "their_card_slot";
            if ($player['player_id'] == $current_player_id || $isSpectator && $player_num == 2) // necessary for spectator mode where the current player is neither of those playing
            {
                $template_variable_prefix = "MY_";
                $card_slot_block_name = "my_card_slot";
            }

            $this->tpl[$template_variable_prefix . "PLAYER_ID"] = $player['player_id'];
            $this->tpl[$template_variable_prefix . "PLAYER_NAME"] = $player['player_name'];
            $this->tpl[$template_variable_prefix . "PLAYER_COLOR"] = $player['player_color'];

            $this->page->reset_subblocks('card_slot');
            
            $this->page->begin_block("fistfulofgold_fistfulofgold", $card_slot_block_name);
        
            for ($i = 1; $i <= $this->game->getNumCardSlots() - 1; $i++)
            {
                $this->page->insert_block($card_slot_block_name, array(
                    "PLAYER_ID" => $player['player_id'],
                    "SLOT_NUMBER" => $i
                ));
            }

            $this->tpl["FINAL_SLOT_NUMBER"] = $this->game->getNumCardSlots();
        }

        $this->page->begin_block("fistfulofgold_fistfulofgold", "gold_zone");
        for ($i = 1; $i <= $this->game->getNumCardSlots() - 1; $i++)
        {
            $this->page->insert_block("gold_zone", array(
                "SLOT_NUMBER" => $i
            ));
        }
        
        $this->tpl['ROUND_LABEL'] = self::_("Round");

        /*********** Do not change anything below this line  ************/
  	}
  }
  

